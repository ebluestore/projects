<?php
  /**#**#**
   * activeCollab license file
   * 
   * NOTE: If you change the content of this file without the written permission 
   * from A51 Development you are violating activeCollab License Agreement and 
   * your license may be terminated!
   *|*|*|*/

  define('LICENSE_KEY', base64_decode('RkxJUE1PREUyMDE0MDEyMk9CTElR'));
  define('LICENSE_UID', '6969696969');
  define('LICENSE_URL', 'localhost');
  define('LICENSE_EXPIRES', '2050-01-01');
  define('LICENSE_PACKAGE', 'corporate');
  define('LICENSE_COPYRIGHT_REMOVED', true);

?>