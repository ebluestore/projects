<?php /* Smarty version Smarty-3.1.12, created on 2015-02-02 11:19:39
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/tracking/views/default/project_tracking/log.tpl" */ ?>
<?php /*%%SmartyHeaderCode:93410474054cf5d4b2e1c56-38427218%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3d5cd86f56da5ed6e75877f2b60bebba32f20447' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/tracking/views/default/project_tracking/log.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '93410474054cf5d4b2e1c56-38427218',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'totals' => 0,
    'active_project' => 0,
    'items' => 0,
    'parent_tasks' => 0,
    'can_manage_items' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54cf5d4b39ac23_33289461',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cf5d4b39ac23_33289461')) {function content_54cf5d4b39ac23_33289461($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_function_empty_slate')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.empty_slate.php';
if (!is_callable('smarty_modifier_json')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_assemble')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_modifier_map')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.map.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time and Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"project_tracking_totals",'module'=>"tracking"),$_smarty_tpl);?>

<?php echo smarty_function_use_widget(array('name'=>"time_expenses_log",'module'=>"tracking"),$_smarty_tpl);?>


<div class="wireframe_content_wrapper">
  <div id="project_tracking_totals"></div>

  <div class="project_time_expenses_wrapper"><div class="project_time_expenses_wrapper_inner">
    <div id="project_time_expenses"></div>
  </div></div>

  <?php echo smarty_function_empty_slate(array('name'=>'time_expenses','module'=>@TRACKING_MODULE),$_smarty_tpl);?>

</div>

<script type="text/javascript">
  $('#project_tracking_totals').projectTrackingTotals({
    'totals' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['totals']->value);?>
,
    'project_id' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getId());?>
,
    'currency' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getCurrency());?>
,
    'refresh_totals_url' : '<?php echo smarty_function_assemble(array('route'=>'project_tracking_get_totals','project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
'
  });

  $('#project_time_expenses').timeExpensesLog({
    'initial_data' : <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['items']->value);?>
,
    'parent_tasks' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['parent_tasks']->value);?>
,
    'currency' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getCurrency());?>
,
    'project_id' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getId());?>
,
    'can_manage_items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['can_manage_items']->value);?>
,
    'mass_update_url' : '<?php echo smarty_function_assemble(array('route'=>'project_tracking_mass_update','project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
'
  });  
</script><?php }} ?>