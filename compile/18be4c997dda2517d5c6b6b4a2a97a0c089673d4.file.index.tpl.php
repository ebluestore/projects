<?php /* Smarty version Smarty-3.1.12, created on 2015-02-04 13:16:41
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/email/views/default/fw_incoming_mailboxes_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:211426305254d21bb990afd0-47786335%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '18be4c997dda2517d5c6b6b4a2a97a0c089673d4' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/email/views/default/fw_incoming_mailboxes_admin/index.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '211426305254d21bb990afd0-47786335',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'mailboxes' => 0,
    'mailboxes_per_page' => 0,
    'total_mailboxes' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54d21bb996c437_71526672',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54d21bb996c437_71526672')) {function content_54d21bb996c437_71526672($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_function_assemble')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_modifier_json')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_image_url')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Incoming Mail<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All Mailboxes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<div id="incoming_mailboxes"></div>

<script type="text/javascript">
  $('#incoming_mailboxes').pagedObjectsList({
    'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'incoming_email_admin_mailboxes'),$_smarty_tpl);?>
', 
    'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['mailboxes']->value);?>
,
    'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['mailboxes_per_page']->value,$_smarty_tpl);?>
, 
    'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_mailboxes']->value,$_smarty_tpl);?>
, 
    'list_items_are' : 'tr', 
    'list_item_attributes' : { 'class' : 'mailbox' }, 
    'columns' : {
      'is_enabled' : '', 
      'name' : App.lang('Name'), 
      'host' : App.lang('Host Name'), 
      'email' : App.lang('Address'), 
      'status' : App.lang('Last Connection Status'), 
      'options' : '' 
    }, 
    'sort_by' : 'name', 
    'empty_message' : App.lang('There are no incoming mailboxes defined'), 
    'listen' : 'incoming_mailbox', 
    'on_add_item' : function(item) {
      var mailbox = $(this);
      
      mailbox.append('<td class="is_enabled">' + 
        '<td class="name"></td>' + 
        '<td class="host"></td>' + 
        '<td class="email"></td>' +  
        '<td class="status"></td>' +  
        '<td class="options"></td>'
      );

      var chc = $('<input type="checkbox" />').attr({
        'on_url' : item['urls']['enable'], 
        'off_url' : item['urls']['disable']
      }).asyncCheckbox({
        'success_event' : 'incoming_mailbox_updated', 
        'success_message' : [ App.lang('Mailbox has been disabled'), App.lang('Mailbox has been enabled') ],
      });
      if(item['is_enabled']) { 
          chc.attr('checked','checked');
      }//if
      chc.appendTo(mailbox.find('td.is_enabled'));

      $('<a></a>').attr('href', item['urls']['list_messages']).text(item['name']).appendTo(mailbox.find('td.name'));
      mailbox.find('td.host').text(App.clean(item['host']));
      mailbox.find('td.email').text(App.clean(item['email']));
      
      if(item['status'] == 1) {
        mailbox.find('td.status').append(App.lang('OK'));
      } else if(item['status'] == 2) {
        mailbox.find('td.status').append(App.lang('Failed'));
      } else {
        mailbox.find('td.status').append(App.lang('Not Checked'));
      } // if

      mailbox.find('td.options')
        .append('<a href="' + item['urls']['view'] + '" class="mailbox_details" title="' + App.lang('View Details') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/preview.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
        .append('<a href="' + item['urls']['edit'] + '" class="edit_mailbox" title="' + App.lang('Change Settings') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
        .append('<a href="' + item['urls']['delete'] + '" class="delete_mailbox" title="' + App.lang('Remove Mailbox') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
      ;

      //mailbox.find('td.options a.mailbox_details').flyout();
      mailbox.find('td.options a.edit_mailbox').flyoutForm({
        'success_event' : 'incoming_mailbox_updated'
      });
      mailbox.find('td.options a.delete_mailbox').asyncLink({
        'confirmation' : App.lang('Are you sure that you want to permanently delete this mailbox? Messages imported through this mailbox will not be removed'), 
        'success_event' : 'incoming_mailbox_deleted', 
        'success_message' : App.lang('Mailbox has been deleted successfully')
      });
    }
  });
</script><?php }} ?>