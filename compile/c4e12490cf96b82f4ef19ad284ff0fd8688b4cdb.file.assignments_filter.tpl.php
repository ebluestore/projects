<?php /* Smarty version Smarty-3.1.12, created on 2015-01-27 01:59:02
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/system/views/default/homescreen_widgets/assignments_filter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:150534763654c6f0e6ed8db0-15494205%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c4e12490cf96b82f4ef19ad284ff0fd8688b4cdb' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/system/views/default/homescreen_widgets/assignments_filter.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '150534763654c6f0e6ed8db0-15494205',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'widget' => 0,
    'assignments' => 0,
    'labels' => 0,
    'project_slugs' => 0,
    'task_url' => 0,
    'task_subtask_url' => 0,
    'todo_url' => 0,
    'todo_subtask_url' => 0,
    'show_group_headers' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54c6f0e7058b16_33656103',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c6f0e7058b16_33656103')) {function content_54c6f0e7058b16_33656103($_smarty_tpl) {?><?php if (!is_callable('smarty_function_use_widget')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_map')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.map.php';
if (!is_callable('smarty_modifier_json')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php echo smarty_function_use_widget(array('name'=>"assignments_list",'module'=>"system"),$_smarty_tpl);?>


<div id="assignments_filter_<?php echo clean($_smarty_tpl->tpl_vars['widget']->value->getId(),$_smarty_tpl);?>
"></div>

<script type="text/javascript">
  $('#assignments_filter_<?php echo clean($_smarty_tpl->tpl_vars['widget']->value->getId(),$_smarty_tpl);?>
').assignmentsList({
    'assignments' : <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['assignments']->value);?>
,
    'labels' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['labels']->value);?>
,
    'project_slugs' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['project_slugs']->value);?>
,
    'task_url' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['task_url']->value);?>
,
    'task_subtask_url' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['task_subtask_url']->value);?>
,
    'todo_url' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['todo_url']->value);?>
,
    'todo_subtask_url' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['todo_subtask_url']->value);?>
,
    'show_group_headers' : <?php if ($_smarty_tpl->tpl_vars['show_group_headers']->value){?>true<?php }else{ ?>false<?php }?>,
    'show_assignment_type' : true,
    'show_no_assignments_message' : true,
    'no_assignments_message' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['widget']->value->getEmptyResultMessage());?>
,
    'no_assignments_message_css_class' : 'homescreen_empty_widget'
  });
</script><?php }} ?>