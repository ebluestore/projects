<?php /* Smarty version Smarty-3.1.12, created on 2015-01-27 16:27:55
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/system/views/default/sharing_settings/sharing_settings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:75051710954c7bc8befa143-00149570%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aac8bc328e738afc1bed5cd8e14a081e70ebcf28' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/system/views/default/sharing_settings/sharing_settings.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '75051710954c7bc8befa143-00149570',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_object' => 0,
    'sharing_data' => 0,
    'logged_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54c7bc8c135620_98776981',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c7bc8c135620_98776981')) {function content_54c7bc8c135620_98776981($_smarty_tpl) {?><?php if (!is_callable('smarty_function_use_widget')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_lang')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_form')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap_fields')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_fields.php';
if (!is_callable('smarty_function_yes_no')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.yes_no.php';
if (!is_callable('smarty_block_wrap')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_radio_field')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.radio_field.php';
if (!is_callable('smarty_function_select_date')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_checkbox')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox.php';
if (!is_callable('smarty_block_textarea_field')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.textarea_field.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
if (!is_callable('smarty_modifier_json')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php echo smarty_function_use_widget(array('name'=>"form",'module'=>"environment"),$_smarty_tpl);?>


<div id="sharing_settings">
  <div id="sharing_setting_sharing_disabled">
    <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true))); $_block_repeat=true; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true)), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
This :type is not shared<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true)), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
    <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true))); $_block_repeat=true; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true)), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
If you wish to show this :type to users that are not registered on the system, you should share it<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true)), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>

    <?php if ($_smarty_tpl->tpl_vars['active_object']->value->getVisibility()=='VISIBILITY_PRIVATE'&&!($_smarty_tpl->tpl_vars['active_object']->value->sharing()->getSharingProfile() instanceof SharedObjectProfile)){?>
      <p id="sharing_settings_visibility_warning"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true),'link_to_manual'=>'http://www.activecollab.com/docs/manuals/tutorials/tips/protect-privacy-of-your-data#use-private-visibility-option')); $_block_repeat=true; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true),'link_to_manual'=>'http://www.activecollab.com/docs/manuals/tutorials/tips/protect-privacy-of-your-data#use-private-visibility-option'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
This :type is marked as <b>Private</b>. If you choose to share it, :type will become visible for all registered users. <a href=":link_to_manual" target="_blank">Click here</a> to learn more<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true),'link_to_manual'=>'http://www.activecollab.com/docs/manuals/tutorials/tips/protect-privacy-of-your-data#use-private-visibility-option'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
    <?php }?>

    <div class="section_button_wrapper" id="sharing_settings_start_sharing_button">
      <a class="section_button" href="#" style="display: inline-block;"><span><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Start Sharing<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></a>
    </div>
  </div>

  <div id="sharing_setting_sharing_enabled">
    <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
This object is shared to public and it's visible on this url:<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
    <p class="public_url"><strong><a href="#" target="_blank"></a></strong></p>

    <dl>
      <dt><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sharing Expires<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</dt>
      <dd id="sharing_settings_property_sharing_expires"></dd>

      <dt><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Comments Enabled<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</dt>
      <dd id="sharing_setting_property_comments_enabled"></dd>

      <dt><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Attachments Enabled<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</dt>
      <dd id="sharing_setting_property_attachments_enabled"></dd>

      <dt><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('object_type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true))); $_block_repeat=true; echo smarty_block_lang(array('object_type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true)), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Reopen :object_type on new comment<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('object_type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true)), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</dt>
      <dd id="sharing_setting_property_reopen_on_new_comment"></dd>
    </dl>

    <div class="section_button_wrapper">
      <a class="section_button first" href="#" style="display: inline-block;" id="sharing_settings_update_sharing_button"><span><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sharing Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></a>
      <a class="section_button last" href="#" style="display: inline-block;" id="sharing_settings_stop_sharing_button"><span><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Stop Sharing<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></a>
    </div>
  </div>

  <div id="sharing_settings_sharing_update">
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['active_object']->value->sharing()->getSettingsUrl())); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_object']->value->sharing()->getSettingsUrl()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_fields', array()); $_block_repeat=true; echo smarty_block_wrap_fields(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <input type="hidden" name="sharing[code]" value="<?php echo clean($_smarty_tpl->tpl_vars['sharing_data']->value['code'],$_smarty_tpl);?>
" />
        <input type="hidden" name="sharing[enabled]" value="true" id="sharing_settings_sharing_enabled"/>

        <div id="sharing_settings_stop">
          <?php echo smarty_function_yes_no(array('name'=>"sharing[unsubscribe_unregistered]",'value'=>0,'label'=>"Unsubscribe all unregistered users"),$_smarty_tpl);?>

        </div>

        <div id="sharing_settings_wrapper" class="slide_down_settings" style="">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'expires_on')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'expires_on'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Expiry Date<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            <div id="sharing_settings_does_not_expire">
              <?php echo smarty_function_radio_field(array('name'=>"sharing[expires]",'checked'=>empty($_smarty_tpl->tpl_vars['sharing_data']->value['expires_on']),'value'=>false,'label'=>"Public Page is Always Available"),$_smarty_tpl);?>

            </div>
            <div id="sharing_settings_does_expire">
              <?php echo smarty_function_radio_field(array('name'=>"sharing[expires]",'checked'=>$_smarty_tpl->tpl_vars['sharing_data']->value['expires_on'],'value'=>true,'label'=>"Public Page is Available Until the Given Date"),$_smarty_tpl);?>

              <div class="slide_down_settings borderless" id="sharing_settings_expire_on_date" <?php if (empty($_smarty_tpl->tpl_vars['sharing_data']->value['expires_on'])){?>style="display: none"<?php }?>>
                <?php echo smarty_function_select_date(array('name'=>"sharing[expires_on]",'value'=>$_smarty_tpl->tpl_vars['sharing_data']->value['expires_on']),$_smarty_tpl);?>

              </div>
            </div>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'expires_on'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


          <?php if ($_smarty_tpl->tpl_vars['active_object']->value->sharing()->supportsComments()){?>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'additional_settings')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'additional_settings'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Additional Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


              <?php echo smarty_function_checkbox(array('name'=>"sharing[comments_enabled]",'checked'=>$_smarty_tpl->tpl_vars['sharing_data']->value['comments_enabled'],'label'=>"Enable Comments",'id'=>"sharing_settings_comments_enabled"),$_smarty_tpl);?>

              <?php echo smarty_function_checkbox(array('name'=>"sharing[attachments_enabled]",'checked'=>$_smarty_tpl->tpl_vars['sharing_data']->value['attachments_enabled'],'label'=>"Enable Attachments",'id'=>"sharing_settings_attachments_enabled"),$_smarty_tpl);?>


              <?php if ($_smarty_tpl->tpl_vars['active_object']->value instanceof IComplete){?>
                <?php echo smarty_function_checkbox(array('name'=>"sharing[comment_reopens]",'checked'=>$_smarty_tpl->tpl_vars['sharing_data']->value['comment_reopens'],'label'=>"Reopen on New Comment",'id'=>"sharing_settings_comment_reopens"),$_smarty_tpl);?>

              <?php }?>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'additional_settings'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php }?>

          <div id="sharing_settings_invitees">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'additional_settings')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'additional_settings'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true))); $_block_repeat=true; echo smarty_block_label(array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true)), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invite people to collaborate on this :type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('type'=>$_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(true)), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              <?php $_smarty_tpl->smarty->_tag_stack[] = array('textarea_field', array('name'=>"sharing[invitees]")); $_block_repeat=true; echo smarty_block_textarea_field(array('name'=>"sharing[invitees]"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_textarea_field(array('name'=>"sharing[invitees]"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              <p class="details"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Enter comma-separated list of email addresses (Invalid ones will be ignored)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'additional_settings'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          </div>
        </div>
      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_fields(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
or <a href="#" id="sharing_settings_cancel_button">Cancel</a><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_object']->value->sharing()->getSettingsUrl()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  </div>
</div>

<script type="text/javascript">

  // encapsulate
  (function () {

    // wrappers
    var wrapper = $('#sharing_settings');
    var enabled_wrapper = wrapper.find('#sharing_setting_sharing_enabled');
    var disabled_wrapper = wrapper.find('#sharing_setting_sharing_disabled');
    var update_wrapper = wrapper.find('#sharing_settings_sharing_update').hide();
    var sharing_settings_wrapper = wrapper.find('#sharing_settings_wrapper');
    var update_form = update_wrapper.find('form:first');
    var sharing_enabled = update_wrapper.find('#sharing_settings_sharing_enabled');
    var invitees_field = update_wrapper.find('#sharing_settings_invitees textarea:first');
    var sharing_settings_stop = update_wrapper.find('#sharing_settings_stop');

    sharing_settings_stop.hide();

    var processing_overlay, confirm_changes;

    /**
     * Puts the form in processing mode
     */
    var start_processing_form = function () {
      if (processing_overlay) {
        return false;
      } // if

      processing_overlay = $('<div class="sharing_settings_processing_overlay"></div>').appendTo(wrapper).css({
        'opacity' : 0,
        'background-image' : 'url(' + App.Wireframe.Utils.indicatorUrl('big') + ')'
      }).animate({
        'opacity' : 1
      });
    }; // start_processing_forms

    /**
     * Stops the form processing
     *
     * @param transition_to
     */
    var stop_processing_form = function (transition_to) {
      if (transition_to) {
        var transition_from;
        if (transition_to.is(enabled_wrapper)) {
          transition_from = update_wrapper
        } else {
          transition_from = enabled_wrapper;
        } // if

        transition_from.slideUp(function () {
          processing_overlay.remove();
          processing_overlay = null;
          transition_to.slideDown();
        });
      } else {
        processing_overlay.animate({
          'opacity' : 0
        }, {
          'complete' : function () {
            processing_overlay.remove();
            processing_overlay = null;
          }
        })
      } // if
    }; // stop_processing_form

    /**
     * Update sharing properties
     *
     * @param Object sharing_properties
     */
    var update_sharing_properties = function (sharing_properties) {
      // update public url
      var public_url = sharing_properties && sharing_properties['urls'] && sharing_properties['urls']['sharing_public'] ? sharing_properties['urls']['sharing_public'] : '';
      enabled_wrapper.find('p.public_url a:first').attr('href', public_url).text(public_url);

      var sharing_expires = sharing_properties && sharing_properties['sharing'] && sharing_properties['sharing']['expires'] ? sharing_properties['sharing']['expires']['formatted_date'] : App.lang('Never');
      enabled_wrapper.find('dd#sharing_settings_property_sharing_expires').text(sharing_expires);

      // comments enabled
      var comments_enabled = sharing_properties && sharing_properties['sharing'] && sharing_properties['sharing']['comments_enabled'] ? App.lang('Yes') : App.lang('No');
      enabled_wrapper.find('dd#sharing_setting_property_comments_enabled').text(comments_enabled);

      // attachments enabled
      var attachments_enabled = sharing_properties && sharing_properties['sharing'] && sharing_properties['sharing']['attachments_enabled'] ? App.lang('Yes') : App.lang('No');
      enabled_wrapper.find('dd#sharing_setting_property_attachments_enabled').text(attachments_enabled);

      // reopen enabled
      var reopen_enabled = sharing_properties && sharing_properties['sharing'] && sharing_properties['sharing']['reopen_on_new_comment'] ? App.lang('Yes') : App.lang('No');
      enabled_wrapper.find('dd#sharing_setting_property_reopen_on_new_comment').text(reopen_enabled);
    }; // update_sharing_properties

    // initially update sharing properties
    update_sharing_properties(<?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_object']->value->describe($_smarty_tpl->tpl_vars['logged_user']->value,true,true));?>
);

    // is shared
    var is_shared = <?php echo clean(smarty_modifier_json($_smarty_tpl->tpl_vars['active_object']->value->sharing()->isShared()),$_smarty_tpl);?>
;
    if (is_shared) {
      enabled_wrapper.show();
      disabled_wrapper.hide();
    } else {
      enabled_wrapper.hide();
      disabled_wrapper.show();
    } // if

    // buttons
    var button_start_sharing = wrapper.find('#sharing_settings_start_sharing_button a');
    var button_cancel_sharing = wrapper.find('#sharing_settings_cancel_button');
    var button_save_changes = wrapper.find('#sharing_settings_sharing_update form div.button_holder button:first');
    var button_update_sharing = wrapper.find('#sharing_settings_update_sharing_button');
    var button_stop_sharing = wrapper.find('#sharing_settings_stop_sharing_button');

    // start sharing button behavior
    button_start_sharing.click(function (event) {
      disabled_wrapper.slideUp(function () {
        sharing_settings_wrapper.show();
        sharing_settings_stop.hide();
        confirm_changes = false;
        sharing_enabled.val('true');
        button_save_changes.text(App.lang('Start Sharing'));
        update_wrapper.slideDown();
      });
      event.preventDefault();
    });

    // update settings button behavior
    button_cancel_sharing.click(function (event) {
      update_wrapper.slideUp(function () {
        if (is_shared) {
          sharing_settings_wrapper.show();
          sharing_settings_stop.hide();
          enabled_wrapper.slideDown();
        } else {
          sharing_settings_wrapper.hide();
          sharing_settings_stop.show();
          disabled_wrapper.slideDown();
        } // if
      });
      event.preventDefault();
    });

    // update settings form behavior
    var input_radio_does_not_expire = wrapper.find('#sharing_settings_does_not_expire input[type=radio]');
    var input_text_does_expire = wrapper.find('#sharing_settings_does_expire div.select_date input[type=text]');
    var input_radio_does_expire = wrapper.find('#sharing_settings_does_expire input[type=radio]');
    var date_picker_expire = wrapper.find('#sharing_settings_expire_on_date');

    input_radio_does_not_expire.click(function() {
      input_text_does_expire.val('');
      date_picker_expire.slideUp('fast');
    });

    input_radio_does_expire.click(function () {
      date_picker_expire.slideDown('fast');
    });

    var comments_enabled = wrapper.find('#sharing_settings_comments_enabled');
    var comment_reopens_parent = wrapper.find('#sharing_settings_comment_reopens').parents('div:first');
    var comment_attachments = wrapper.find('#sharing_settings_attachments_enabled').parents('div:first');

    comments_enabled.change(function () {
      if (comments_enabled.is(':checked')) {
        comment_reopens_parent.show();
        comment_attachments.show();
      } else {
        comment_reopens_parent.hide();
        comment_attachments.hide();
      } // if
    });

    comments_enabled.trigger('change');

    // save changes
    button_save_changes.click(function () {
      if (confirm_changes) {
        if (!confirm(App.lang('Are you sure that you want to stop sharing this item?'))) {
          return false;
        } // if
      } // if

      start_processing_form();

      update_form.ajaxSubmit({
        'url' : App.extendUrl(update_form.attr('action'), {
          'async' : 1
        }),
        'success' : function (response) {
          is_shared = response && typeof response['sharing'] == 'object';
          if (!is_shared) {
            enabled_wrapper.hide();
            update_wrapper.hide();
          } // if

          update_sharing_properties(response);

          invitees_field.val('');

          if (response && response['event_names'] && response['event_names']['updated']) {
            App.Wireframe.Events.trigger(response['event_names']['updated'], [response]);
          } // if

          stop_processing_form(is_shared ? enabled_wrapper : disabled_wrapper);
        },
        'error' : function (response) {
          stop_processing_form();
        }
      })
      return false;
    });

    // update sharing
    button_update_sharing.click(function (event) {
      enabled_wrapper.slideUp(function () {
        button_save_changes.text(App.lang('Update Sharing Settings'));
        update_wrapper.slideDown();
      });

      sharing_enabled.val('true');
      confirm_changes = false;
      event.preventDefault();
    });

    // stop sharing
    button_stop_sharing.click(function (event) {
      enabled_wrapper.slideUp(function () {
        sharing_settings_wrapper.hide();
        sharing_settings_stop.show();
        button_save_changes.text(App.lang('Stop sharing'));
        update_wrapper.slideDown();
      });

      sharing_enabled.val('');
      confirm_changes = true;
      event.preventDefault();
    });
  }());
</script><?php }} ?>