<?php /* Smarty version Smarty-3.1.12, created on 2015-03-02 01:52:03
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/tasks/views/default/weekly_completed_tasks_reports/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:140250439954f3c2433116c0-47048734%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5ae374bf38de06a6974f0aae54b10a7b62099fd7' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/tasks/views/default/weekly_completed_tasks_reports/index.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '140250439954f3c2433116c0-47048734',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'saved_filters' => 0,
    'task_segments' => 0,
    'companies' => 0,
    'projects' => 0,
    'active_projects' => 0,
    'project_categories' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54f3c24341e860_10258146',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54f3c24341e860_10258146')) {function content_54f3c24341e860_10258146($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_function_assemble')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_block_lang')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_button')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.button.php';
if (!is_callable('smarty_modifier_map')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.map.php';
if (!is_callable('smarty_modifier_json')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Completed Tasks (Weekly)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Completed Tasks (Weekly)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>'flot','module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>

<?php echo smarty_function_use_widget(array('name'=>"ui_date_picker",'module'=>"environment"),$_smarty_tpl);?>

<?php echo smarty_function_use_widget(array('name'=>"filter_criteria",'module'=>"reports"),$_smarty_tpl);?>


<div id="weekly_completed_tasks_report" class="filter_criteria">
  <form action="<?php echo smarty_function_assemble(array('route'=>'weekly_completed_tasks_reports_run'),$_smarty_tpl);?>
" method="get" class="expanded">

    <!-- Filter Picker -->
    <div class="filter_criteria_head">
      <div class="filter_criteria_head_inner">
        <div class="filter_criteria_picker">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Filter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:
          <select>
            <option value=""><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Custom<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
          </select>
        </div>

        <div class="filter_criteria_run"><?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('type'=>"submit",'class'=>"default")); $_block_repeat=true; echo smarty_block_button(array('type'=>"submit",'class'=>"default"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Run<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('type'=>"submit",'class'=>"default"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
        <div class="filter_criteria_options" style="display: none"></div>
      </div>
    </div>

    <div class="filter_criteria_body"></div>
  </form>

  <div class="filter_results"></div>
</div>

<script type="text/javascript">
  $('#weekly_completed_tasks_report').filterCriteria({
    'on_result_links' : function(response, data, links) {
      App.Wireframe.Utils.reportRegisterExportLinks('<?php echo smarty_function_assemble(array('route'=>'weekly_completed_tasks_reports_export'),$_smarty_tpl);?>
', response, data, links);
    },
    'criterions' : {
      'tasks_segment_filter' : {
        'label' : App.lang('Tasks Segment'),
        'choices' : {
          'any' : App.lang('Any'),
          'selected' : {
            'label' : App.lang('Selected Segment ...'),
            'prepare' : App['Wireframe']['Utils']['dataFilters']['prepareSelectTaskSegment']
          }
        }
      },
      'date_filter' : {
        'label' : App.lang('Date Range'),
        'choices' : {
          'any' : App.lang('All Time'),
          'selected_range' : {
            'label' : App.lang('Selected Date Range ...'),
            'prepare' : App['Wireframe']['Utils']['dataFilters']['prepareSelectDateRange'],
            'get_name' : function() {
              return 'date';
            },
            'get_value' : function(f) {
              return typeof(f) =='object' && f ? [f['date_from'], f['date_to']] : null;
            }
          }
        }
      },
      'project_filter' : {
        'label' : App.lang('Projects'),
        'choices' : {
          'any' : App.lang('Any Project'),
          'active' : App.lang('Active Projects'),
          'completed' : App.lang('Completed Projects'),
          'category' : {
            'label' : App.lang('From Category ...'),
            'prepare' : App['Wireframe']['Utils']['dataFilters']['prepareSelectProjectCategory']
          },
          'client' : {
            'label' : App.lang('For Client ...'),
            'prepare' : App['Wireframe']['Utils']['dataFilters']['prepareSelectCompany'],
            'get_name' : function(c) {
              return 'project_client_id';
            },
            'get_value' : function(f, c) {
              return typeof(f) == 'object' && f ? f['project_client_id'] : null;
            }
          },
          'selected' : {
            'label' : App.lang('Selected Projects ...'),
            'prepare' : App['Wireframe']['Utils']['dataFilters']['prepareSelectProjects']
          }
        }
      }
    },
    'filters' : <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['saved_filters']->value);?>
,
    'new_filter_url' : '<?php echo smarty_function_assemble(array('route'=>'weekly_completed_tasks_reports_add'),$_smarty_tpl);?>
',
    'can_add_filter' : true,
    'on_show_results' : function(response, data, form_data) {
      var results_wrapper = $(this);

      if(response) {
        var weekly_completed_graph = $('<div id="weekly_completed_tasks_graph" class="plot centered" style="width: 800px; height: 400px;"></div>').appendTo(results_wrapper);
        var weekly_completed_table = $('<table id="weekly_completed_tasks_table" class="common auto report_data">' +
          '<thead>' +
          '<tr>' +
          '<td class="week">' + App.lang('Week') + '</td>' +
          '<td class="total_tasks center">' + App.lang('New Tasks') + '</td>' +
          '</tr>' +
          '</thead>' +
          '<tbody></tbody>' +
          '</table>').appendTo(results_wrapper);

        var completed_tasks_data = [];
        var rows = '';

        App.each(response, function(k, v) {
          completed_tasks_data.push([ v.week_end_timestamp * 1000, v.completed_tasks ]);

          rows += '<tr>' +
            '<td class="week">' + App.lang(':year, Week :week', {
            'year' : v.year,
            'week' : v.week
          }) + '</td>' +
            '<td class="completed_tasks tasks_count">' + v.completed_tasks + '</td>' +
            '</tr>';
        });

        weekly_completed_table.find('tbody').append(rows);

        /**
         * Highlight years with different colors
         *
         * @param axes
         * @return Array
         */
        var prepare_grid_marking = function(axes) {
          var markings = [];

          var starts_with = new Date(axes.xaxis.min).setTimezoneOffset(0);
          var reference_date = Date.parse(starts_with.getFullYear() + '/1/1').setTimezoneOffset(0);

          var interval = 1;

          do {
            var from_timestamp = reference_date.getTime();

            reference_date = reference_date.next().year();

            var to_timestamp = reference_date.getTime();

            if(interval % 2) {
              markings.push({
                'xaxis' : {
                  'from' : from_timestamp,
                  'to' : to_timestamp < axes.xaxis.max ? to_timestamp : axes.xaxis.max
                }
              });
            } // if

            interval++;
          } while(to_timestamp < axes.xaxis.max);

          return markings;
        } // prepare_grid_marking

        $.plot(weekly_completed_graph, [ {
          'label' : App.lang('Completed Tasks'),
          'data' : completed_tasks_data
        } ], {
          'series' : {
            'bars' : {
              'show' : true
            }
          },
          'legend' : {
            'noColumns' : 1
          },
          'xaxis' : {
            'mode' : 'time',
            'timeformat': "%d/%m/%y"
          },
          'grid' : {
            'markings' : prepare_grid_marking
          }
        });
      } // if
    },
    'data' : {
      'task_segments' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['task_segments']->value);?>
,
      'companies' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['companies']->value);?>
,
      'projects' : <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['projects']->value);?>
,
      'active_projects' : <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['active_projects']->value);?>
,
      'project_categories' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['project_categories']->value);?>

    }
  });
</script><?php }} ?>