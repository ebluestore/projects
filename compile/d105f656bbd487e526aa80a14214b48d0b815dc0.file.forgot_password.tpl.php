<?php /* Smarty version Smarty-3.1.12, created on 2015-01-29 07:53:23
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/authentication/notifications/email/forgot_password.tpl" */ ?>
<?php /*%%SmartyHeaderCode:178689152554c9e6f31e1256-26422407%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd105f656bbd487e526aa80a14214b48d0b815dc0' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/authentication/notifications/email/forgot_password.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '178689152554c9e6f31e1256-26422407',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'language' => 0,
    'context' => 0,
    'recipient' => 0,
    'sender' => 0,
    'style' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54c9e6f3234a53_09731878',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c9e6f3234a53_09731878')) {function content_54c9e6f3234a53_09731878($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_notification_wrapper')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/email/helpers/block.notification_wrapper.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Reset Your Password<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

================================================================================
<?php $_smarty_tpl->smarty->_tag_stack[] = array('notification_wrapper', array('title'=>'Reset Your Password','context'=>$_smarty_tpl->tpl_vars['context']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value)); $_block_repeat=true; echo smarty_block_notification_wrapper(array('title'=>'Reset Your Password','context'=>$_smarty_tpl->tpl_vars['context']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('reset_url'=>$_smarty_tpl->tpl_vars['context']->value->getResetPasswordUrl(),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('reset_url'=>$_smarty_tpl->tpl_vars['context']->value->getResetPasswordUrl(),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Visit <a href=":reset_url" style=":link_style">this page</a> to reset your password. This link will be valid for 2 days only<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('reset_url'=>$_smarty_tpl->tpl_vars['context']->value->getResetPasswordUrl(),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_notification_wrapper(array('title'=>'Reset Your Password','context'=>$_smarty_tpl->tpl_vars['context']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>