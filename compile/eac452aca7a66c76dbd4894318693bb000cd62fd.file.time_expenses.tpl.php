<?php /* Smarty version Smarty-3.1.12, created on 2015-02-02 11:19:39
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/tracking/views/default/empty_slates/time_expenses.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14880082954cf5d4b39ef40-14342938%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eac452aca7a66c76dbd4894318693bb000cd62fd' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/tracking/views/default/empty_slates/time_expenses.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14880082954cf5d4b39ef40-14342938',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54cf5d4b3dfd58_31307537',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cf5d4b3dfd58_31307537')) {function content_54cf5d4b3dfd58_31307537($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_image_url')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><div id="empty_slate_time_expenses" class="empty_slate">
  <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
About Time and Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>

  <ul class="icon_list">
    <li>
      <img src="<?php echo smarty_function_image_url(array('name'=>"icons/32x32/time-entry.png",'module'=>@TRACKING_MODULE),$_smarty_tpl);?>
" class="icon_list_icon" alt="" />
      <span class="icon_list_title"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time Records<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
      <span class="icon_list_description"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Use time records to track how much time you have spent working on this project. Time can be tracked directly for the project from this page, or on a task level via a timer widget that is available on the task details page<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</span>
    </li>

    <li>
      <img src="<?php echo smarty_function_image_url(array('name'=>"icons/32x32/expense.png",'module'=>@TRACKING_MODULE),$_smarty_tpl);?>
" class="icon_list_icon" alt="" />
      <span class="icon_list_title"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
      <span class="icon_list_description"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Track expenses to know how much you have additionaly spent on this project, apart from the tracked time. This feature is great for tracking travel expenses, material costs, hosting and domain names and all other, non-time project expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</span>
    </li>

    <li>
      <img src="<?php echo smarty_function_image_url(array('name'=>"empty-slates/help.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" class="icon_list_icon" alt="" />
      <span class="icon_list_title"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Track Time with activeCollab Timer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
      <span class="icon_list_description"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<a href="http://blankrefer.com/?https://www.activecollab.com/timer.html">activeCollab Timer</a> is a free application which automatically tracks time while you are working on your tasks and projects. It will run in the background and keep track of how much time you spend working on each task. When you are done, click on the Submit button and the activeCollab Timer will automatically create a time record in your activeCollab system. The Timer application is available for Windows, Mac and Linux<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</span>
    </li>
  </ul>
</div><?php }} ?>