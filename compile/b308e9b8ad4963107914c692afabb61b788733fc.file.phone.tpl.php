<?php /* Smarty version Smarty-3.1.12, created on 2015-02-26 09:23:55
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/layouts/phone.tpl" */ ?>
<?php /*%%SmartyHeaderCode:171008068254eee62b78ca05-44035020%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b308e9b8ad4963107914c692afabb61b788733fc' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/layouts/phone.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '171008068254eee62b78ca05-44035020',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'wireframe' => 0,
    'breadcrumb_name' => 0,
    'breadcrumb' => 0,
    'content_for_layout' => 0,
    'wireframe_action' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54eee62b895b58_58415554',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54eee62b895b58_58415554')) {function content_54eee62b895b58_58415554($_smarty_tpl) {?><?php if (!is_callable('smarty_function_brand')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.brand.php';
if (!is_callable('smarty_function_touch')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.touch.php';
if (!is_callable('smarty_function_template_vars_to_js')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.template_vars_to_js.php';
if (!is_callable('smarty_modifier_json')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><!DOCTYPE html> 
<html> 
	<head>
		<?php if (AngieApplication::isInDevelopment()||AngieApplication::isInDebugMode()){?>
	    <meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="pragma" content="no-cache">
  	<?php }?>
  	
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  	<title><?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getPageTitle(lang('Index')),$_smarty_tpl);?>
</title>
  	
  	<link rel="shortcut icon" href="<?php echo smarty_function_brand(array('what'=>'favicon'),$_smarty_tpl);?>
" type="image/x-icon" />
  	
  	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo smarty_function_touch(array('what'=>'icon','size'=>'144x144'),$_smarty_tpl);?>
" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo smarty_function_touch(array('what'=>'icon','size'=>'114x114'),$_smarty_tpl);?>
" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo smarty_function_touch(array('what'=>'icon','size'=>'72x72'),$_smarty_tpl);?>
" />
		<link rel="apple-touch-icon-precomposed" href="<?php echo smarty_function_touch(array('what'=>'icon'),$_smarty_tpl);?>
" />
  	
  	<link rel="apple-touch-startup-image" href="<?php echo smarty_function_touch(array('what'=>'startup'),$_smarty_tpl);?>
">
  	
  	<meta name="viewport" content="width=640, initial-scale=0.5, target-densitydpi=device-dpi" />
  	<meta name="apple-mobile-web-app-capable" content="yes" />
  	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
  	
  	<link rel="stylesheet" href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedStylesheetsUrl(AngieApplication::INTERFACE_PHONE,AngieApplication::CLIENT_IPHONE),$_smarty_tpl);?>
" type="text/css" media="screen" id="style_main_css"/>
    <script type="text/javascript" src="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedJavaScriptUrl(AngieApplication::INTERFACE_PHONE,AngieApplication::CLIENT_IPHONE),$_smarty_tpl);?>
"></script>
    
    <?php echo smarty_function_template_vars_to_js(array('wireframe'=>$_smarty_tpl->tpl_vars['wireframe']->value),$_smarty_tpl);?>

  </head>
  <body class="phone" onorientationchange="App.Wireframe.iPhoneOrientationChange.fix();">
    <div data-role="page" data-theme="f">
    	<div data-role="header" data-theme="f">
    	<?php if ($_smarty_tpl->tpl_vars['wireframe']->value->breadcrumbs->hasBackPage()){?>
    	  <a href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->breadcrumbs->getBackPageUrl(),$_smarty_tpl);?>
" id="wireframe_back_button" title="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->breadcrumbs->getBackPageText(),$_smarty_tpl);?>
" data-rel="back" data-icon="arrow-l">Back</a>
    	<?php }?>
    	<?php if ($_smarty_tpl->tpl_vars['wireframe']->value->actions->getPrimary() instanceof WireframeAction){?>
    		<style type="text/css">
	        .ui-header a[data-rel='primary'] { background: transparent url('<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->actions->getPrimary()->getIcon(AngieApplication::INTERFACE_PHONE),$_smarty_tpl);?>
') 0 0 no-repeat !important; }
	    	</style>
    	  <a href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->actions->getPrimary()->getUrl(),$_smarty_tpl);?>
" id="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->actions->getPrimary()->getId(),$_smarty_tpl);?>
" data-rel="primary"><?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->actions->getPrimary()->getText(),$_smarty_tpl);?>
</a>
    	<?php }?>
    	<h1><?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getPageTitle(lang('Index')),$_smarty_tpl);?>
</h1>
    	</div>
    	
    	<?php if ($_smarty_tpl->tpl_vars['wireframe']->value->breadcrumbs->count()>2){?>
    	<div data-role="breadcrumbs" class="wireframe_breadcrumbs">
    	  <ul>
    	  <?php  $_smarty_tpl->tpl_vars['breadcrumb'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['breadcrumb']->_loop = false;
 $_smarty_tpl->tpl_vars['breadcrumb_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['wireframe']->value->breadcrumbs; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['breadcrumb']->key => $_smarty_tpl->tpl_vars['breadcrumb']->value){
$_smarty_tpl->tpl_vars['breadcrumb']->_loop = true;
 $_smarty_tpl->tpl_vars['breadcrumb_name']->value = $_smarty_tpl->tpl_vars['breadcrumb']->key;
?>
    	    <li id="breadcrumb_<?php echo clean($_smarty_tpl->tpl_vars['breadcrumb_name']->value,$_smarty_tpl);?>
">
    	    <?php if ($_smarty_tpl->tpl_vars['breadcrumb']->value['url']){?>
    	    	<a href="<?php echo clean($_smarty_tpl->tpl_vars['breadcrumb']->value['url'],$_smarty_tpl);?>
"><?php if ($_smarty_tpl->tpl_vars['breadcrumb_name']->value!='home'){?><?php echo clean($_smarty_tpl->tpl_vars['breadcrumb']->value['text'],$_smarty_tpl);?>
<?php }?></a>
    	    <?php }else{ ?>
    	      <?php echo clean($_smarty_tpl->tpl_vars['breadcrumb']->value['text'],$_smarty_tpl);?>

    	    <?php }?>
    	    </li>
    	  <?php } ?>
    	  </ul>
    	</div>
    	<?php }?>
    
    	<div data-role="content" class="wireframe_content"><?php echo $_smarty_tpl->tpl_vars['content_for_layout']->value;?>
</div>
    
      <?php if ($_smarty_tpl->tpl_vars['wireframe']->value->actions->count()>0){?>
      <style type="text/css">
    	<?php  $_smarty_tpl->tpl_vars['wireframe_action'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['wireframe_action']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['wireframe']->value->actions; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['wireframe_action']->key => $_smarty_tpl->tpl_vars['wireframe_action']->value){
$_smarty_tpl->tpl_vars['wireframe_action']->_loop = true;
?>
        #<?php echo clean($_smarty_tpl->tpl_vars['wireframe_action']->value->getId(),$_smarty_tpl);?>
 .ui-icon { background: url('<?php echo clean($_smarty_tpl->tpl_vars['wireframe_action']->value->getIcon(AngieApplication::INTERFACE_PHONE),$_smarty_tpl);?>
') 50% 50% no-repeat; }
      <?php } ?>
    	</style>
      
      <div data-role="footer" data-position="fixed" class="wireframe_actions" data-theme="g">
      	 <div data-role="navbar">
      		<ul>
      		<?php  $_smarty_tpl->tpl_vars['wireframe_action'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['wireframe_action']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['wireframe']->value->actions; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['wireframe_action']->key => $_smarty_tpl->tpl_vars['wireframe_action']->value){
$_smarty_tpl->tpl_vars['wireframe_action']->_loop = true;
?>
            <li><a href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe_action']->value->getUrl(),$_smarty_tpl);?>
" id="<?php echo clean($_smarty_tpl->tpl_vars['wireframe_action']->value->getId(),$_smarty_tpl);?>
" data-icon="custom"></a></li>
          <?php } ?>
      		</ul>
      	</div>
      </div>
      <?php }?>
    </div>
    
    <script type="text/javascript">
    	$(document).ready(function() {
  			App.Wireframe.MobileBreadCrumbs.init('wireframe_breadcrumbs', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['wireframe']->value->breadcrumbs);?>
);
  			App.Wireframe.ConfirmationDialogs.init();

        $('.wireframe_logo').parent().addClass('show_logo');
  		});
  		
  		// prevents links from full-screen apps from opening in mobile safari
  		(function(document,navigator,standalone) {
				if((standalone in navigator) && navigator[standalone]) {
					var curnode, location=document.location, stop=/^(a|html)$/i;
					document.addEventListener('click', function(e) {
						curnode=e.target;
						while(!(stop).test(curnode.nodeName)) {
							curnode=curnode.parentNode;
						} // while
						
						var attr = $(curnode).attr('data-rel');
						
						var is_back_button = false;
						if(typeof attr !== 'undefined' && attr !== false && attr == 'back') {
							is_back_button = true;
						} // if
						
						var is_select_box = false;
						if(typeof curnode.href !== 'undefined' && curnode.href.substr(-1) == "#") {
							is_select_box = true;
						} // if
						
						// Conditions to do this only on links to your own app. If you want all links, use if('href' in curnode) instead.
						if(
							!is_back_button && 																						 // back button is not affected
							!is_select_box && 																						 // select box isn't affected, too
							'href' in curnode && 																					 // is a link
							(chref=curnode.href).replace(location.href,'').indexOf('#') && // is not an anchor
							(!(/^[a-z\+\.\-]+:/i).test(chref) ||                       		 // either does not have a proper scheme (relative links)
								chref.indexOf(location.protocol+'//'+location.host)===0) 		 // or is in the same protocol and domain
						) {
							e.preventDefault();
							location.href = curnode.href;
						} else if(is_back_button && !is_select_box) {
							window.location.href = curnode.href;
						} // if
					}, false);
				} // if
			})(document, window.navigator, 'standalone');
		</script>
  </body>
</html><?php }} ?>