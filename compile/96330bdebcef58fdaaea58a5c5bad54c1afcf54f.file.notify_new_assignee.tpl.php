<?php /* Smarty version Smarty-3.1.12, created on 2015-01-30 15:37:36
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/assignees/notifications/email/notify_new_assignee.tpl" */ ?>
<?php /*%%SmartyHeaderCode:52922230354cba540212611-20692027%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '96330bdebcef58fdaaea58a5c5bad54c1afcf54f' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/assignees/notifications/email/notify_new_assignee.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '52922230354cba540212611-20692027',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'context' => 0,
    'language' => 0,
    'title_lang' => 0,
    'context_view_url' => 0,
    'recipient' => 0,
    'sender' => 0,
    'style' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54cba5402f1313_21121851',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cba5402f1313_21121851')) {function content_54cba5402f1313_21121851($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_excerpt')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.excerpt.php';
if (!is_callable('smarty_block_lang')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_notification_wrapper')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/email/helpers/block.notification_wrapper.php';
if (!is_callable('smarty_function_notification_assignment_reassigned')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/assignees/helpers/function.notification_assignment_reassigned.php';
if (!is_callable('smarty_block_notification_wrap_body')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/email/helpers/block.notification_wrap_body.php';
?><?php echo clean($_smarty_tpl->tpl_vars['context']->value->assignees()->getNotificationSubjectPrefix(),$_smarty_tpl);?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('name'=>smarty_modifier_excerpt($_smarty_tpl->tpl_vars['context']->value->getName()),'type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(false,$_smarty_tpl->tpl_vars['language']->value),'language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('name'=>smarty_modifier_excerpt($_smarty_tpl->tpl_vars['context']->value->getName()),'type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(false,$_smarty_tpl->tpl_vars['language']->value),'language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
You are now Responsible for ':name' :type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('name'=>smarty_modifier_excerpt($_smarty_tpl->tpl_vars['context']->value->getName()),'type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(false,$_smarty_tpl->tpl_vars['language']->value),'language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

================================================================================
<?php $_smarty_tpl->smarty->_tag_stack[] = array('notification_wrapper', array('title'=>$_smarty_tpl->tpl_vars['title_lang']->value,'context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value)); $_block_repeat=true; echo smarty_block_notification_wrapper(array('title'=>$_smarty_tpl->tpl_vars['title_lang']->value,'context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	<?php echo smarty_function_notification_assignment_reassigned(array('context'=>$_smarty_tpl->tpl_vars['context']->value,'reassigned_by_name'=>$_smarty_tpl->tpl_vars['sender']->value->getDisplayName(),'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(true,$_smarty_tpl->tpl_vars['language']->value),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value),$_smarty_tpl);?>

  
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('notification_wrap_body', array()); $_block_repeat=true; echo smarty_block_notification_wrap_body(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['context']->value->getBody();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_notification_wrap_body(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_notification_wrapper(array('title'=>$_smarty_tpl->tpl_vars['title_lang']->value,'context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>