<?php /* Smarty version Smarty-3.1.12, created on 2015-02-26 08:48:46
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/source/views/default/repository/_repository_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:206126043554eeddee174210-02027236%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f2b721ebc357b66b20ef323f117394c171953901' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/source/views/default/repository/_repository_form.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '206126043554eeddee174210-02027236',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'disable_url_and_type' => 0,
    'types' => 0,
    'repository_data' => 0,
    'update_types' => 0,
    'logged_user' => 0,
    'project_object_repository' => 0,
    'repository_test_connection_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54eeddee2460b9_86222692',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54eeddee2460b9_86222692')) {function content_54eeddee2460b9_86222692($_smarty_tpl) {?><?php if (!is_callable('smarty_block_wrap')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_select_repository_type')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/source/helpers/function.select_repository_type.php';
if (!is_callable('smarty_block_lang')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_text_field')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_password_field')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.password_field.php';
if (!is_callable('smarty_function_select_repository_update_type')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/source/helpers/function.select_repository_update_type.php';
if (!is_callable('smarty_function_select_visibility')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_visibility.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_function_image_url')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_submit')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?>  <div class="fields_wrapper">
		<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'type')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		  <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'repositoryType')); $_block_repeat=true; echo smarty_block_label(array('for'=>'repositoryType'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Repository Type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'repositoryType'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		  <?php echo smarty_function_select_repository_type(array('disabled'=>$_smarty_tpl->tpl_vars['disable_url_and_type']->value,'name'=>'repository[type]','id'=>'repositoryType','data'=>$_smarty_tpl->tpl_vars['types']->value,'selected'=>$_smarty_tpl->tpl_vars['repository_data']->value['repositorytype']),$_smarty_tpl);?>

		  <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Please choose the repository type you wish to connect on.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
		<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'type'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		
		<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'name')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'name'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		  <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'repositoryName','required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('for'=>'repositoryName','required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'repositoryName','required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		  <?php echo smarty_function_text_field(array('name'=>'repository[name]','value'=>$_smarty_tpl->tpl_vars['repository_data']->value['name'],'id'=>'repositoryName','class'=>'title required','maxlength'=>"150"),$_smarty_tpl);?>

		<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'name'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		
		<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'url')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'url'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		  <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'repositoryUrl','required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('for'=>'repositoryUrl','required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Repository URL or directory<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'repositoryUrl','required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		  <?php echo smarty_function_text_field(array('name'=>'repository[repository_path_url]','disabled'=>$_smarty_tpl->tpl_vars['disable_url_and_type']->value,'value'=>$_smarty_tpl->tpl_vars['repository_data']->value['repository_path_url'],'id'=>'repositoryUrl','class'=>'title required'),$_smarty_tpl);?>

		  <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Please enter the root path to the repository.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
		<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'url'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


    <div id="sourceAuthenticateWrapper">
      <div class="col">
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'username')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'username'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'repositoryUsername')); $_block_repeat=true; echo smarty_block_label(array('for'=>'repositoryUsername'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Username<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'repositoryUsername'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php echo smarty_function_text_field(array('name'=>'repository[username]','style'=>'width:250px','value'=>$_smarty_tpl->tpl_vars['repository_data']->value['username'],'id'=>'repositoryUsername'),$_smarty_tpl);?>

      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'username'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>

      <div class="col">
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'password')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'password'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'repositoryPassword')); $_block_repeat=true; echo smarty_block_label(array('for'=>'repositoryPassword'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Password<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'repositoryPassword'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php echo smarty_function_password_field(array('name'=>'repository[password]','value'=>$_smarty_tpl->tpl_vars['repository_data']->value['password'],'id'=>'repositoryPassword'),$_smarty_tpl);?>

      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'password'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
		<div class="clear"></div>
		
		<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'type')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		  <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'repositoryUpdateType')); $_block_repeat=true; echo smarty_block_label(array('for'=>'repositoryUpdateType'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Commit History Update Type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'repositoryUpdateType'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		  <?php echo smarty_function_select_repository_update_type(array('name'=>'repository[update_type]','id'=>'repositoryUpdateType','data'=>$_smarty_tpl->tpl_vars['update_types']->value,'selected'=>$_smarty_tpl->tpl_vars['repository_data']->value['updatetype']),$_smarty_tpl);?>

		<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'type'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

			
		<?php if ($_smarty_tpl->tpl_vars['logged_user']->value->canSeePrivate()){?>
		  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'visibility')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'visibility'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		    <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'repositoryVisibility')); $_block_repeat=true; echo smarty_block_label(array('for'=>'repositoryVisibility'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Visibility<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'repositoryVisibility'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		    <?php echo smarty_function_select_visibility(array('name'=>'repository[visibility]','value'=>$_smarty_tpl->tpl_vars['repository_data']->value['visibility'],'object'=>$_smarty_tpl->tpl_vars['project_object_repository']->value),$_smarty_tpl);?>

		  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'visibility'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		<?php }else{ ?>
		  <input type="hidden" name="repository[visibility]" value="1"/>
		<?php }?>
  </div>
  
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="test_connection">
      <input type="hidden" value="<?php echo clean($_smarty_tpl->tpl_vars['repository_test_connection_url']->value,$_smarty_tpl);?>
" id="repository_test_connection_url" />
      <button type="button"><span><span><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Test Connection<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></span></button>
      <img src="<?php echo smarty_function_image_url(array('name'=>"layout/bits/indicator-loading-normal.gif",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt='' />    
    </div>
    
    <div class="submit_repository">
	    <?php if ($_smarty_tpl->tpl_vars['project_object_repository']->value->isNew()){?>
	      <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Add Repository<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    <?php }else{ ?>
	      <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    <?php }?>
    </div>
  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<script type="text/javascript">
	var set_form = function () {
		var type = $('#repositoryType').val();
		if (type == 'SvnRepository') {
			$('.col').show();
		} else {
			$('.col').hide();
		}
	};

	$(document).ready (function () {
		set_form();
		$('#repositoryType').change(function () {
			set_form();
		});
	});
</script><?php }} ?>