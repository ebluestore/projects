<?php /* Smarty version Smarty-3.1.12, created on 2015-02-26 09:24:17
         compiled from "/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/system/views/phone/projects/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53529745254eee6411a1aa8-25176624%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6f313f73373e74217c4abe798d1956c271b5231d' => 
    array (
      0 => '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/modules/system/views/phone/projects/index.tpl',
      1 => 1392207864,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '53529745254eee6411a1aa8-25176624',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'favorite_projects' => 0,
    'project' => 0,
    'other_projects' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_54eee641239940_26847326',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54eee641239940_26847326')) {function content_54eee641239940_26847326($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_image_url')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_lang')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_assemble')) include '/var/zpanel/hostdata/ebluesto/public_html/projects_ebluestore_com/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Projects<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Active<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="projects">
	<div class="favorite_projects">
		<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
			<li data-role="list-divider"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/listviews/projects-icon.png",'module'=>@SYSTEM_MODULE,'interface'=>AngieApplication::INTERFACE_PHONE),$_smarty_tpl);?>
" class="divider_icon" alt=""><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Favorite Projects<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
			<?php if (is_foreachable($_smarty_tpl->tpl_vars['favorite_projects']->value)){?>
			  <?php  $_smarty_tpl->tpl_vars['project'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['project']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['favorite_projects']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['project']->key => $_smarty_tpl->tpl_vars['project']->value){
$_smarty_tpl->tpl_vars['project']->_loop = true;
?>
		 			<li><a href="<?php echo clean($_smarty_tpl->tpl_vars['project']->value->getViewUrl(),$_smarty_tpl);?>
"><img class="ui-li-icon" src="<?php echo clean($_smarty_tpl->tpl_vars['project']->value->avatar()->getUrl(IProjectAvatarImplementation::SIZE_BIG),$_smarty_tpl);?>
" alt=""/><?php echo clean($_smarty_tpl->tpl_vars['project']->value->getName(),$_smarty_tpl);?>
</a></li>
			  <?php } ?>
			<?php }else{ ?>
				<li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no favorite projects<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
			<?php }?>
		</ul>
	</div>
	
	<div class="other_active_projects">
		<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
			<li data-role="list-divider"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/listviews/projects-icon.png",'module'=>@SYSTEM_MODULE,'interface'=>AngieApplication::INTERFACE_PHONE),$_smarty_tpl);?>
" class="divider_icon" alt=""><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Other Active Projects<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
			<?php if (is_foreachable($_smarty_tpl->tpl_vars['other_projects']->value)){?>
			  <?php  $_smarty_tpl->tpl_vars['project'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['project']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['other_projects']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['project']->key => $_smarty_tpl->tpl_vars['project']->value){
$_smarty_tpl->tpl_vars['project']->_loop = true;
?>
		 			<li><a href="<?php echo clean($_smarty_tpl->tpl_vars['project']->value->getViewUrl(),$_smarty_tpl);?>
"><img class="ui-li-icon" src="<?php echo clean($_smarty_tpl->tpl_vars['project']->value->avatar()->getUrl(IProjectAvatarImplementation::SIZE_BIG),$_smarty_tpl);?>
" alt=""/><?php echo clean($_smarty_tpl->tpl_vars['project']->value->getName(),$_smarty_tpl);?>
</a></li>
			  <?php } ?>
			<?php }else{ ?>
				<li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no other active projects<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
			<?php }?>
		</ul>
	</div>
	
	<div class="archived_objects">
  	<a href="<?php echo smarty_function_assemble(array('route'=>'projects_archive'),$_smarty_tpl);?>
" data-role="button" data-theme="k">Completed Projects</a>
  </div>
</div><?php }} ?>