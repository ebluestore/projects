<?php

// We need admin controller
AngieApplication::useController ( 'admin' );

/**
 * Manages Quick Add Plus settings
 * 
 * @package custom.modules.quick_add_plus
 * @subpackage controllers
 */
class QuickAddPlusAdminController extends AdminController {
	
	/**
	 * Main Quick Add Plus Module Admin page
	 *
	 */
	function index() {
		if ($this->request->isSubmitted ()) {
			$quick_add_plus_settings = $this->request->post('quick_add_plus');
			ConfigOptions::setValue('quick_add_plus_settings', $quick_add_plus_settings, true);
		} // if
		
		$quick_add_plus_data = ConfigOptions::getValue('quick_add_plus_settings');
		$object_types = array('active' => 'Active', 'completed' => 'Completed', 'both' => 'Both');
		$this->response->assign ( array ('quick_add_plus_data' => $quick_add_plus_data, 'object_types' => $object_types) );
	} // index

}