<?php

  /**
   * on_admin_panel event handler
   * 
   * @package activeCollab.modules.tasks
   * @subpackage handlers
   */

  /**
   * Handle on_admin_panel event
   * 
   * @param AdminPanel $admin_panel
   */
  function quick_add_plus_handle_on_admin_panel(AdminPanel &$admin_panel) {
    $admin_panel->addToProjects('quick_add_plus_admin', lang('Quick Add Plus'), Router::assemble('quick_add_plus_admin'), AngieApplication::getImageUrl('module.png', QUICK_ADD_PLUS_MODULE));
  } // quick_add_plus_handle_on_admin_panel