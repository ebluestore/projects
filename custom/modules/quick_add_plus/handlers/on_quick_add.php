<?php

  /**
   * Quick Add Plus module on_quick_add event handler
   *
   * @package custom.modules.quick_add_plus
   * @subpackage handlers
   */
  
  /**
   * Handle on quick add event
   *
   * @param NamedList $items
   * @param NamedList $subitems
   * @param array $map
   * @param User $logged_user
   * @param DBResult $projects 
   * @param DBResult $companies
   * @param string $interface
   */
  function quick_add_plus_handle_on_quick_add($items, $subitems, &$map, $logged_user, $projects, $companies, $interface = AngieApplication::INTERFACE_DEFAULT) {
  	
  	 if(is_foreachable($projects)) {

  	 	$is_tracking_module_loaded = AngieApplication::isModuleLoaded('tracking');
  	 	$item_id = 'subtask';
  	 	$count = 0;
  		foreach($projects as $project) {
  			if(Tasks::canAdd($logged_user, $project)) {
  				$map[$item_id][] = 'project_' . $project->getId();
  			} // if
  			
  			if($is_tracking_module_loaded && $project->tracking()->canAdd($logged_user)) {
  				$count++;
  			}
  		} // foreach
  	 	
	  	if($count > 0) {
		  	$time_entry_data = $items->get('time_entry');
		  	$time_entry_data['url'] = Router::assemble('quick_add_time_records_add', array('project_slug' => '--PROJECT-SLUG--'));
		  	$items->add('time_entry', $time_entry_data);
		  	
		  	$time_expense_data = $items->get('expense');
		  	$time_expense_data['url'] = Router::assemble('quick_add_expenses_add', array('project_slug' => '--PROJECT-SLUG--'));
		  	$items->add('expense', $time_expense_data);
	  	} 
  		
  		if(isset($map[$item_id])) {
  			
  			// Adding subtaks icon next to the Task icon in QA screen 
  			$items->addAfter($item_id, array(
  					'text'	=> lang('Subtask'),
  					'title' => lang('Add a Subtask'),
  					'dialog_title' => lang('Add Subtask to the :name project'),
  					'icon' => $interface == AngieApplication::INTERFACE_DEFAULT ? AngieApplication::getImageUrl('icons/32x32/task.png', TASKS_MODULE) : AngieApplication::getImageUrl('icons/96x96/task.png', TASKS_MODULE, $interface),
  					'url' => Router::assemble('quick_add_subtask', array('project_slug' => '--PROJECT-SLUG--')),
  					'group' => QuickAddCallback::GROUP_PROJECT,
            'handler_settings' => array(
            'width' => 'narrow'
             ),
  					'event'	=> 'subtask_created'),
  					'task');
	  		} // if
  	} // if
  	
  	
  } // quick_add_plus_handle_on_quick_add