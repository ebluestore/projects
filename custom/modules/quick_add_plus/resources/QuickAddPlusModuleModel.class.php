<?php

// Include applicaiton specific model base
require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';

/**
 * Quick Add Plus module model definition
 *
 * @package custom.modules.quick_add_plus
 * @subpackage resources
 */
class QuickAddPlusModuleModel extends ActiveCollabModuleModel {
	
	/*
     * Construct Quick Add Plus module model definition
     *
     * @param Quick Add Plus $parent
     */
	function __construct(QuickAddPlusModule $parent) {
		parent::__construct ( $parent );
	
	}
	/**
	 * Load initial framework data
	 *
	 * @param string $environment
	 */
	function loadInitialData($environment = null) {
		$quick_add_plus_settings = array('object_type' => 'both');				
		$this->addConfigOption('quick_add_plus_setings', $quick_add_plus_setings);
		parent::loadInitialData ( $environment );
	} // loadInitialData
	
}   