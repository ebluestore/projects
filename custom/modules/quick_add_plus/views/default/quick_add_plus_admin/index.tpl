{title}Quick Add Plus Module Configuration{/title}
{add_bread_crumb}Quick Add Plus{/add_bread_crumb}

  <div class="content_stack_wrapper">
    {form action=Router::assemble('quick_add_plus_admin') method=post}
    <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Settings{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
	        {wrap field=object_type}
	          {label for=object_type}Select task type{/label}
	          <select class="required" name="quick_add_plus[object_type]" id='type'>
	          {foreach $object_types as $type => $object_type}
	            {if $type == $quick_add_plus_data.object_type}
	            <option value="{$type}" selected="selected">{lang}{$object_type}{/lang}</option>
	            {else}
	            <option value="{$type}">{lang}{$object_type}{/lang}</option>
	            {/if}
	          {/foreach}
	          </select>
	          <p class="aid">{lang}Selected type of tasks will be available in select parent dropdown while adding subtask, time entry & expense{/lang}.</p>
	        {/wrap}
      	</div>
	</div>
      {wrap_buttons}
    	{submit}Save Changes{/submit}
      {/wrap_buttons}
  {/form}
</div>