
{if is_foreachable($tasks_grouped_by_milestone)}
	<div id="add_sub_task">
	  {form method=post id=quick_add_subtask action=$quick_add_subtask_url enctype="multipart/form-data" }
	    <div class="fields_wrapper">
	        {wrap field=parent_id}
				{select_parent name='subtask[parent_id]' value=$subtask_data.parent_id label='Select Parent' items=$tasks_grouped_by_milestone group_id_name_map=$milestone_id_name_map project=$active_project user=$logged_user optional=false required=true include_project=false }
			{/wrap}
			
			{wrap field=body}
			    {label for=subtaskSummary required=yes}Summary{/label}
			    {text_field name='subtask[body]' value=$subtask_data.body class='title required' id=subtaskSummary}
			{/wrap}
			
			{wrap field=user_id}
	    	    {label for="($subtasks_id)_select_assignee"}Assignee{/label} 
	    	    {select_assignee name='subtask[assignee_id]' value=$subtask_data.assignee_id parent=$task user=$logged_user id="{$subtasks_id}_select_assignee"}
	        {/wrap}
	
	
			{if $subtask->usePriority()}
				{wrap field=priority}	
				     <div class="subtask_attribute subtask_priority">
				    	{label for="($subtasks_id)_task_priority"}Priority{/label} {select_priority name='subtask[priority]' value=$subtask_data.priority id="{$subtasks_id}_task_priority"}
				  	</div>
				{/wrap} 	
		  	{/if}
        
	        {if $subtask->useLabels()}
	        	{wrap field=label}
			        <div class="subtask_attribute subtask_label">
			          {label for="($subtasks_id)_label"}Label{/label} {select_label name='subtask[label_id]' value=$subtask_data.label_id type=get_class($subtask->label()->newLabel()) id="{$subtasks_id}_label" user=$logged_user can_create_new=false}
			        </div>
			     {/wrap}   
	        {/if}
			
			{wrap field=due_on}
	    		{label for=subtaskDueOn}Due on{/label}
	    		{select_date name='subtask[due_on]' value=$subtask_data.due_on id=subtaskDueOn}
	  		{/wrap}
			
			
		</div>
	  
	    {wrap_buttons}
	    	{submit}Add SubTask{/submit}
	    {/wrap_buttons}
	  {/form}
	</div>
{else}
	<p class="empty_page">{lang}There is no Task. Can not create new SubTask.{/lang}</p>
{/if}
