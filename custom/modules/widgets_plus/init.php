<?php

/**
 * Init widgets_plus module
 *
 * @package custom.modules.widgets_plus
 */

define ( 'WIDGETS_PLUS_MODULE', 'widgets_plus' );
define ( 'WIDGETS_PLUS_MODULE_PATH', CUSTOM_PATH . '/modules/widgets_plus' );
define ( 'SINGLE_COLUMN', 'single' );
define ( 'EXTRA_WIDE_COLUMN', 'extra_wide' );

AngieApplication::setForAutoload ( array (
	//homescreenWidget
	'RssFeedHomescreenWidget' => WIDGETS_PLUS_MODULE_PATH . '/models/homescreen_widgets/RssFeedHomescreenWidget.class.php', 
	'MessagesHomescreenWidget' => WIDGETS_PLUS_MODULE_PATH . '/models/homescreen_widgets/MessagesHomescreenWidget.class.php', 
	'RemotecontentHomescreenWidget' => WIDGETS_PLUS_MODULE_PATH . '/models/homescreen_widgets/RemotecontentHomescreenWidget.class.php', 
	'TextHomescreenWidget' => WIDGETS_PLUS_MODULE_PATH . '/models/homescreen_widgets/TextHomescreenWidget.class.php', 
	'NotesHomescreenWidget' => WIDGETS_PLUS_MODULE_PATH . '/models/homescreen_widgets/NotesHomescreenWidget.class.php', 
	//homescreenTab
	'SingleHomescreenTab' => WIDGETS_PLUS_MODULE_PATH . '/models/homescreen_tabs/SingleHomescreenTab.class.php', 
	'RightWideHomescreenTab' => WIDGETS_PLUS_MODULE_PATH . '/models/homescreen_tabs/RightWideHomescreenTab.class.php', 
	'LeftWideHomescreenTab' => WIDGETS_PLUS_MODULE_PATH . '/models/homescreen_tabs/LeftWideHomescreenTab.class.php' ) 
); 