<?php

/**
 * Widgets_plus module definition
 *
 * @package custom.modules.Widgets_plus
 * @subpackage models
 */
class WidgetsPlusModule extends AngieModule {
	
	/**
	 * Plain module name
	 *
	 * @var string
	 */
	protected $name = 'widgets_plus';
	
	/**
	 * Module version
	 *
	 * @var string
	 */
	protected $version = '4.1';
	
	// ---------------------------------------------------
	//  Events and Routes
	// ---------------------------------------------------
	

	/**
	 * Define module routes
	 */
	function defineRoutes() {
		Router::map ( 'widgets_plus', 'admin/tools/widgets_plus', array ('controller' => 'widgets_plus', 'action' => 'index' ) );
		Router::map ( 'widgets_plus_download', 'admin/tools/widgets_plus/download', array ('controller' => 'widgets_plus', 'action' => 'download' ) );
		Router::map ( 'widgets_plus_save_messages', 'admin/tools/widgets_plus/save', array ('controller' => 'widgets_plus', 'action' => 'save' ) );
	}// defineRoutes
	

	/**
	 * Define event handlers
	 */
	function defineHandlers() {
		EventsManager::listen ( 'on_admin_panel', 'on_admin_panel' );
		EventsManager::listen ( 'on_homescreen_widget_types', 'on_homescreen_widget_types' );
		EventsManager::listen ( 'on_hourly', 'on_hourly' );
		EventsManager::listen ( 'on_daily', 'on_daily' );
		EventsManager::listen ( 'on_homescreen_tab_types', 'on_homescreen_tab_types' );
	} // defineHandlers
	

	// ---------------------------------------------------
	//  (Un)Install
	//---------------------------------------------------
	

	function uninstall() {
		parent::uninstall ();
		$motd_table = TABLE_PREFIX . 'message_of_the_day';
		if (DB::tableExists ( $motd_table )) {
			DB::execute ( "DROP TABLE $motd_table " );
		} //if
		

		$homescreen_table = TABLE_PREFIX . 'homescreen_widgets';
		if (DB::tableExists ( $homescreen_table )) {
			DB::execute ( "DELETE FROM $homescreen_table WHERE type IN('MessagesHomescreenWidget','NotesHomescreenWidget','RemotecontentHomescreenWidget','RssFeedHomescreenWidget','TextHomescreenWidget')" );
		} //if
		$homescreen_tab_table = TABLE_PREFIX . 'homescreen_tabs';
		if (DB::tableExists ( $homescreen_tab_table )) {
			DB::execute ( "DELETE FROM $homescreen_tab_table WHERE type IN('LeftWideHomescreenTab','RightWideHomescreenTab','SingleHomescreenTab')" );
		} //if
	

	}
	/**
	 * Get module display name
	 *
	 * @return string
	 */
	function getDisplayName() {
		return lang ( 'Widgets Plus' );
	} // getDisplayName
	

	/**
	 * Return module description
	 *
	 * @return string
	 */
	function getDescription() {
		return lang ( 'Put your homescreen to use with awesome widgets' );
	} // getDescription
	

	/**
	 * Return module uninstallation message
	 *
	 * @return string
	 */
	function getUninstallMessage() {
		return lang ( 'Module will be deactivated' );
	} // getUninstallMessage


}