<?php

// Include applicaiton specific model base
require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';

/**
 * widgets_plus module model definition
 *
 * @package custom.modules.Widgets_plus
 * @subpackage resources
 */
class WidgetsPlusModuleModel extends ActiveCollabModuleModel {
	
	/*
     * Construct Widgets_plus module model definition
     *
     * @param Widgets_plus $parent
     */
	function __construct(WidgetsPlusModule $parent) {
		parent::__construct ( $parent );
		
		if (! DB::tableExists ( TABLE_PREFIX . 'message_of_the_day' )) {
			$this->addModel ( DB::createTable ( 'message_of_the_day' )->addColumns ( array (DBIdColumn::create (), DBStringColumn::create ( 'message_type', 50 ), DBStringColumn::create ( 'message', 255 ), DBDateColumn::create ( 'last_shown_on' ) ) ) );
		}
	
	}
	/**
	 * Load initial framework data
	 *
	 * @param string $environment
	 */
	function loadInitialData($environment = null) {
		$results ['inspiration'] = $this->getInspirationData ();
		$results ['trick'] = $this->getTricksData ();
		
		DB::beginWork ( 'Loading Initial Inspirations/Tricks @ ' . __CLASS__ );
		$batch = DB::batchInsert ( TABLE_PREFIX . 'message_of_the_day', array ('message_type', 'message' ) );
		if (is_foreachable ( $results )) {
			foreach ( $results as $message_type => $messages ) {
				if (is_foreachable ( $messages )) {
					foreach ( $messages as $message ) {
						$batch->insert ( $message_type, $message );
					}
				}
			}
			$batch->done ();
		}
		
		DB::commit ( 'Inspitations/Tricks has been loaded @ ' . __CLASS__ );
		parent::loadInitialData ( $environment );
	} // loadInitialData
	

	function getInspirationData() {
		
		$data = array ('Productivity is being able to do things that you were never able to do before.', 'Programmer - an organism that turns coffee into software.', 'There is no substitute for guts.', 'Greatest barrier in success is fear of failure.', 'The difference between the impossible and the possible lies in a person.', 'The only way to keep a good reputation is to continuously earn it.', 'Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.', 'We are what we think.', 'Faith will move mountains.', 'What you become is more important than what you accomplish.', 'Champions in any field have made a habit of doing what others find boring or uncomfortable.', 'Victory belongs to the most persevering.', 'They can, because they think they can.', 'Inspiration is perishable.' );
		return $data;
	}
	
	function getTricksData() {
		$data = array ('Keep switching between activeCollab and another system? Create a new homescreen tab, add single column widget and fill it with remote content URL.', 'Use assignment filters to keep a tab on your upcoming tasks.', 'Don\'t check emails in the morning.', 'Do a daily stand up meeting with your team.' );
		return $data;
	}

}   