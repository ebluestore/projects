<?php

/**
 * widgets_plus module on_daily event handler
 *
 * @package custom.modules.widgetplus
 * @subpackage handlers
 */

/**
 * Daily display inspiration
 */
function widgets_plus_handle_on_daily() {
	//clear cache
	$results = HomescreenWidgets::find ( array ('conditions' => array ('type = ?', 'MessagesHomescreenWidget' ) ) );
	if($results instanceof DBResult){
		if ($results->count () > 0) {
			$widget = array_shift ( $results->toArray () );
			$widget->fetch_message ();
		}
	}
} // widgets_plus_handle_on_daily