<?php

/**
 * on_homescreen_tab_types event handler
 * 
 * @package custom.modules.widgets_plus
 * @subpackage handlers
 */

/**
 * Handle on_homescreen_tab_types event
 * 
 * @param unknown_type $types
 */
function widgets_plus_handle_on_homescreen_tab_types(&$types) {
	$types [] = new SingleHomescreenTab ();
	$types [] = new RightWideHomescreenTab ();
	$types [] = new LeftWideHomescreenTab ();

} // widgets_plus_handle_on_homescreen_tab_types