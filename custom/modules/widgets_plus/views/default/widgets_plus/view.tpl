	<table class="form form_field validate_callback validate_days_off" id="message_schedule">
	<tr>
		<th class="message_type">{label}Type{/label}</th>
		<th class="message">{label}Message{/label}</th>
		<th class="date">{label}Date{/label}</th>
		<th></th>
	</tr>

	{if is_foreachable($message_list)} 
	{foreach from=$message_list item=message key=i}
	<tr class="message_row {cycle values='odd,even'}">
		<td class="message_type">{radio_field
		name="message[{$i}][message_type]" value="inspiration"
		pre_selected_value=$message.message_type label="Inspiration"}
		{radio_field name="message[{$i}][message_type]" value="trick"
		pre_selected_value=$message.message_type label="Tips"}</td>
		<td class="message">{text_field name='message[{$i}][message]'
		value=$message.message class='title'}</td>
		<td class="date">{select_date name="message[{$i}][last_shown_on]"
		value=$message.last_shown_on}</td>
		<td class="delete" ><a><img src="{image_url name="
			icons/12x12/delete.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK} " /></a></td>
	</tr>
	{/foreach} {/if}
</table>
<div>{button id="add_btn"}Add New Message{/button}
<span style="float: right">{button id="zap_btn"}Remove All Messages{/button}</span>
</div>
<br/>
{wrap_buttons}
  	  {submit id="save_btn"}Save All Changes{/submit}
{/wrap_buttons}

<script type="text/javascript">
$('#zap_btn').click(function(){ 
	$('#message_schedule tr').remove();  
});

$('#add_btn').click(function(){

var row = $('<tr class="message_row">' + 
           '<td class="message_type">'+
					'<input type="radio"  value="inspiration"> Inspiration'+
					'<input type="radio"  value="trick"> Tips'+
			'</td>'+
			'<td class="message"><input type="text" class="title" name="message[][message]" /></td>'+
          '<td class="date"><div class="select_date"><input class="input_text input_date" name="message[][last_shown_on]" value="" autocomplete="off" /></div></td>' +
          '<td class="delete"><a><img src="'+App.Wireframe.Utils.imageUrl('icons/12x12/delete.png', 'environment', 'default')+'"></a></td>'+
        '</tr>');
      var date_options = {
        	dateFormat : "yy/mm/dd",
      		minDate : new Date(),
      		maxDate : new Date("2050/01/01"),
      		showAnim : "blind",
      		duration : 0,
      		changeYear: true,
  			showOn: "both",
      		buttonImage: App.Wireframe.Utils.imageUrl('icons/16x16/calendar.png', 'system'),
  			buttonImageOnly: true,
            buttonText : App.lang('Select Date'),
  			firstDay: $('#workweekFirstWeekDay').val(),
      		changeYear: true,
      		hideIfNoPrevNext : true,
  			yearRange: "2000:2050"
        };
        row.find('td.date input').datepicker(date_options);
   
		$('#message_schedule').append(row);     

});

$('.delete').click(function(){
 $(this).parent('tr').remove();
});


$("#save_btn").click(function(){
  var message_list = [];
  $('.message_row').each(function() {
    var row = $(this);
    message_list.push({
      'message' : jQuery.trim(row.find('td.message input').val()),
      'message_type' : jQuery.trim(row.find('td.message_type input:radio:checked').val()),
      'date' : jQuery.trim(row.find('td.date input').val())
    });
  });
  
  var post_data = {};
  post_data.messages = message_list;
  $.ajax({
  	  type: "POST",
  	  data: post_data,
  	  url: "{Router::assemble('widgets_plus_save_messages')}",
      success: App.Wireframe.Flash.success('Messages saved'),
      error: {literal} function(x, s, e) { console.log(s); console.log(e); App.Wireframe.Flash.error('Could not save messages'); } {/literal}
   })
});

</script>