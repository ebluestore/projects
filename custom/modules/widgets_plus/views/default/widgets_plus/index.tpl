{title}Widget Plus{/title} 
{add_bread_crumb}Widget Plus Messages{/add_bread_crumb}

<div class="content_stack_wrapper">

<div class="content_stack_element">
<div class="content_stack_element_info"><h3>{lang}Configure Widgets{/lang}</h3></div>
<div class="content_stack_element_body">
<ul>
<li>{lang}Go to "Home Screens" within Administration.{/lang}</li>
<li>{lang}Click on a home screen name to configure it.{/lang}</li>
<li>{lang}Add a new widget from here. RSS / Remote Content / Messages / Text etc will be under Widgets Plus group.{/lang}</li>
<li>{lang}You can also add a new home screen tab - and select Full Width or Two Column layout added by Widgets Plus.{/lang}</li>
<li>{lang}Configure messages shown in the Message of the Day widget below.{/lang}</li>
</ul>  
</div>
</div>


<div class="content_stack_element">
<div class="content_stack_element_info"><h3>{lang}Import / Export Messages{/lang}</h3></div>
<div class="content_stack_element_body">
{button id="import_csv" title="Import from CSV"}{lang}Import from CSV{/lang}{/button}
{button id="export_csv" title="Export to CSV"}Export to CSV{/button}
	<div class="upload_to_flyout import_messages" style="margin: 20px 5px;">
	{form action=Router::assemble('widgets_plus') method='post' enctype='multipart/form-data' class="import_form"} 
	{wrap field=messages} {file_field name="messages" label="Select CSV File"} {/wrap}
	{submit}{lang}Import Messages{/lang}{/submit} {/form}
	</div>
</div>
</div>

<div class="content_stack_element last">
<div class="content_stack_element_info"><h3>{lang}Manage Messages{/lang}</h3></div>
<div class="content_stack_element_body">
	{include file=get_view_path('view', 'widgets_plus', $smarty.const.WIDGETS_PLUS_MODULE)}
</div>
</div>
<script type="text/javascript">
    // handle on importing csv (upload/review)
    var upload_to_flyout_wrapper = $('div.import_messages.upload_to_flyout');
    $('#import_csv').click(function() {
      upload_to_flyout_wrapper.slideToggle('fast');
    });
    $('#export_csv').click(function() {
      window.open("{Router::assemble('widgets_plus_download')}", "_blank" );
    });

    
</script>