<?php

/**
 * Homescreen tab with two column
 * 
 * @package custom.modules.widgets_plus
 * @subpackage models
 */
class LeftWideHomescreenTab extends WidgetsHomescreenTab {
	
	/**
	 * This home screen tab does accepts widgets
	 *
	 * @var boolean
	 */
	protected $accept_widgets = true;
	
	/**
	 * Column definitions (none)
	 *
	 * @var array
	 */
	
	protected $columns = array (1 => EXTRA_WIDE_COLUMN, 2 => HomescreenTab::NARROW_COLUMN );
	
	/**
	 * Return homescreen tab description
	 * 
	 * @return string
	 */
	function getDescription() {
		return lang ( 'Two Columns, Left Column is Wide' );
	} // getDescription


}