<?php

/**
 * Widgets_plus home screen widget implementation
 * 
 * @package custom.modules.widgets_plus
 * @subpackage models
 */
class NotesHomescreenWidget extends HomescreenWidget {
	
	/**
	 * Return widget name
	 * 
	 * @return string
	 */
	function getName() {
		return lang ( 'Note' );
	} // getName
	

	/**
	 * Return widget description
	 * 
	 * @return string
	 */
	function getDescription() {
		return lang('Show a sticky note on your homescreen');
	} // getDescription
	

	/**
	 * Return group name for widgets of this type
	 * 
	 * @return string
	 */
	function getGroupName() {
		return lang ( 'Widgets Plus' );
	} // getGroupName
	

	/**
	 * Return widget title
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderTitle(IUser $user, $widget_id, $column_wrapper_class = null) {
		
		return "";
	
	} // renderTitle
	

	/**
	 * Return widget body
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderBody(IUser $user, $widget_id, $column_wrapper_class = null) {
		
		$title = $this->getNoteTitle ();
		$note = $this->getNoteMessage ();
		return '<div class="note"><h3 class="head"><span class="head_inner">' . $title . '</span></h3><p class="body_note">' . $note . '</p></div>';
	
	} // renderBody
	

	// ---------------------------------------------------
	//  Options
	// ---------------------------------------------------
	/**
	 * Returns true if this widget has additional options
	 * 
	 * @return boolean
	 */
	function hasOptions() {
		return true;
	} // hasOptions
	

	/**
	 * Render widget options form section
	 * 
	 * @param IUser $user
	 * @return string
	 */
	function renderOptions(IUser $user) {
		$view = SmartyForAngie::getInstance ()->createTemplate ( AngieApplication::getViewPath ( 'notes_options', 'homescreen_widgets', WIDGETS_PLUS_MODULE, AngieApplication::INTERFACE_DEFAULT ) );
		$view->assign ( array ('widget' => $this, 'user' => $user, 'widget_data' => array ('note_title' => $this->getNoteTitle (), 'note_message' => $this->getNoteMessage () ) ) );
		return $view->fetch ();
	} // renderOptions
	

	/**
	 * Bulk set widget attributes
	 * 
	 * @param array $attributes
	 */
	function setAttributes($attributes) {
		$this->setNoteMessage ( isset ( $attributes ['note_message'] ) ? $attributes ['note_message'] : null );
		$this->setNoteTitle ( isset ( $attributes ['note_title'] ) ? $attributes ['note_title'] : null );
		parent::setAttributes ( $attributes );
	} // setAttributes
	

	/**
	 * Return Text message
	 * 
	 * @return string
	 */
	function getNoteMessage() {
		return $this->getAdditionalProperty ( 'note_message' );
	} // getNoteMessage
	

	/**
	 * Set welcome message
	 * 
	 * @param string $value
	 * @return string
	 */
	function setNoteMessage($value) {
		return $this->setAdditionalProperty ( 'note_message', $value );
	} // setNoteMessage
	

	/**
	 * Return Text message
	 * 
	 * @return string
	 */
	function getNoteTitle() {
		return $this->getAdditionalProperty ( 'note_title' );
	} // getNoteTitle
	

	/**
	 * Set welcome message
	 * 
	 * @param string $value
	 * @return string
	 */
	function setNoteTitle($value) {
		return $this->setAdditionalProperty ( 'note_title', $value );
	} // setNoteTitle
}