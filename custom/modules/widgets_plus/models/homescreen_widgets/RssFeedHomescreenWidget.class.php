<?php

/**
 * Widgets_plus home screen widget implementation
 * 
 * @package custom.modules.widgets_plus
 * @subpackage models
 */
class RssFeedHomescreenWidget extends HomescreenWidget {
	
	var $feed_per_url = 5;
	/**
	 * Return widget name
	 * 
	 * @return string
	 */
	function getName() {
		
		return lang ( 'RSS Feed' );
	} // getName
	

	/**
	 * Return widget description
	 * 
	 * @return string
	 */
	function getDescription() {
		return lang ( 'Show articles from an RSS feed' );
	} // getDescription
	

	/**
	 * Return group name for widgets of this type
	 * 
	 * @return string
	 */
	function getGroupName() {
		return lang ( 'Widgets Plus' );
	} // getGroupName

	
	function getCacheFile() {
		return CACHE_PATH .'/'.$this->getCacheKey() . '_feed';
	}
	
	/**
	 * Return widget title
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderTitle(IUser $user, $widget_id, $column_wrapper_class = null) {
		
		return "";
	
	} // renderTitle
	

	/**
	 * Return widget body
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderBody(IUser $user, $widget_id, $column_wrapper_class = null) {
		
		$file = $this->getCacheFile();
		$feed = array();
		if (is_file($file)) {
			$feed = unserialize ( file_get_contents ( $this->getCacheFile() ) );
		}
		if (empty ( $feed )) {
			$feed = $this->write_fetch_rss_feed();
		} //if
		
		$result .= '<h3 class="head">';
		$result .= '<span class="head_inner">' . strtoupper ( $this->getLabel () ) . '</span></h3>';
		if (is_foreachable ( $feed[$this->getRssurl()] )) {
			$i = 0;
			$result .= '<ul>';
			foreach ( $feed[$this->getRssurl()]  as $item ) {
				$title = str_replace ( ' & ', ' &amp; ', $item ['title'] );
				$link = $item ['link'];
				$date = date ( 'l F d, Y', strtotime ( $item ['date'] ) );
				$result .= '<li><a href="' . $link . '" title="' . $title . '">' . $title . '</a></li>';
			} //foreach
			$result .= '</ul>';
		} //if
		
		return $result;
	} // renderBody
	

	// --------------------------------------------------
	//  Options
	// ---------------------------------------------------
	/**
	 * Returns true if this widget has additional options
	 * 
	 * @return boolean
	 */
	function hasOptions() {
		return true;
	} // hasOptions
	

	/**
	 * Render widget options form section
	 * 
	 * @param IUser $user
	 * @return string
	 */
	function renderOptions(IUser $user) {
		$view = SmartyForAngie::getInstance ()->createTemplate ( AngieApplication::getViewPath ( 'rss_options', 'homescreen_widgets', WIDGETS_PLUS_MODULE, AngieApplication::INTERFACE_DEFAULT ) );
		$view->assign ( array ('widget' => $this, 'user' => $user, 'widget_data' => array ('label' => $this->getLabel (), 'rss_url' => $this->getRssurl () ) ) );
		
		return $view->fetch ();
	} // renderOptions
	
	function setAttributes($attributes) {
		$this->setLabel ( isset ( $attributes ['label'] ) ? $attributes ['label'] : null );
		$this->setRssurl ( isset ( $attributes ['rss_url'] ) ? $attributes ['rss_url'] : null );
		parent::setAttributes ( $attributes );
	} // setAttributes
	

	function getLabel() {
		return $this->getAdditionalProperty ( 'label' );
	} // getLabel
	

	function setLabel($value) {
		return $this->setAdditionalProperty ( 'label', $value );
	} // setLabel
	

	function getRssurl() {
		return $this->getAdditionalProperty ( 'rss_url' );
	} // getRssurl
	

	function setRssurl($value) {
		return $this->setAdditionalProperty ( 'rss_url', $value );
	} // setRssurl
	

	function fetch_rss_feed($rss_url) {
		$rss = new SimpleXMLElement ( $rss_url, null,true);
		$feed = array ();
		foreach ( $rss->channel->item as $node ) {
			$node = get_object_vars ( $node );
			$item = array ('title' => ( string ) $node ['title'], 'link' => ( string ) $node ['link'], 'date' => ( string ) $node ['pubDate'] );
			array_push ( $feed, $item );
			if (count($feed) >= $this->feed_per_url) {
				break;
			}
		}
		return $feed;
	}
	
	public static function write_fetch_rss_feed() {
		$feeds = array();
		$results = HomescreenWidgets::find ( array ('conditions' => array ('type = ?', 'RssFeedHomescreenWidget' ) ) );
	    if($results instanceof DBResult){
	      foreach ($results as $result) {
	      	$url = $result->getRssurl ();
	      	$feed = $result->fetch_rss_feed ( $url );
	        $feeds[$url] = $feed;
	      }
	     file_put_contents ( $result->getCacheFile(), serialize ( $feeds ) , FILE_APPEND);
	    }
	    return $feeds;
    }
}