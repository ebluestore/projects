<?php

/**
 * Widget_plus home screen widget implementation
 * 
 * @package custom.modules.widgets_plus
 * @subpackage models
 */
class TextHomescreenWidget extends HomescreenWidget {
	
	/**
	 * Return widget name
	 * 
	 * @return string
	 */
	function getName() {
		return lang ( 'Text / HTML' );
	} // getName
	

	/**
	 * Return widget description
	 * 
	 * @return string
	 */
	function getDescription() {
		return lang('Shows any Text or HTML content');
	} // getDescription
	

	/**
	 * Return group name for widgets of this type
	 * 
	 * @return string
	 */
	function getGroupName() {
		return lang ( 'Widgets Plus' );
	} // getGroupName
	

	/**
	 * Return widget title
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderTitle(IUser $user, $widget_id, $column_wrapper_class = null) {
		$title = $this->getTextTitle ();
		if ($title == '') {
			$title = ' ';
		}
		return $title;
	
	} // renderTitle
	

	/**
	 * Return widget body
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderBody(IUser $user, $widget_id, $column_wrapper_class = null) {
		$view = $this->getTextMessage ();
		$view = HTML::toRichText ( $view );
		return '<div class="text"> ' . $view . '</div>';
	} // renderBody
	

	// ---------------------------------------------------
	//  Options
	// ---------------------------------------------------
	/**
	 * Returns true if this widget has additional options
	 * 
	 * @return boolean
	 */
	function hasOptions() {
		return true;
	} // hasOptions
	

	/**
	 * Render widget options form section
	 * 
	 * @param IUser $user
	 * @return string
	 */
	function renderOptions(IUser $user) {
		$view = SmartyForAngie::getInstance ()->createTemplate ( AngieApplication::getViewPath ( 'text_message_options', 'homescreen_widgets', WIDGETS_PLUS_MODULE, AngieApplication::INTERFACE_DEFAULT ) );
		
		$view->assign ( array ('widget' => $this, 'user' => $user, 'widget_data' => array ('text_title' => $this->getTextTitle (), 'text_message' => $this->getTextMessage () ) ) );
		return $view->fetch ();
	} // renderOptions
	

	/**
	 * Bulk set widget attributes
	 * 
	 * @param array $attributes
	 */
	function setAttributes($attributes) {
		$this->setTextMessage ( isset ( $attributes ['text_message'] ) ? $attributes ['text_message'] : null );
		$this->setTextTitle ( isset ( $attributes ['text_title'] ) ? $attributes ['text_title'] : null );
		parent::setAttributes ( $attributes );
	} // setAttributes
	

	/**
	 * Return Text message
	 * 
	 * @return string
	 */
	function getTextMessage() {
		return $this->getAdditionalProperty ( 'text_message' );
	} // getTextMessage
	

	/**
	 * Set welcome message
	 * 
	 * @param string $value
	 * @return string
	 */
	function setTextMessage($value) {
		return $this->setAdditionalProperty ( 'text_message', $value );
	} // setTextMessage
	

	/**
	 * Return Text message
	 * 
	 * @return string
	 */
	function getTextTitle() {
		return $this->getAdditionalProperty ( 'text_title' );
	} // getTextTitle
	

	/**
	 * Set welcome message
	 * 
	 * @param string $value
	 * @return string
	 */
	function setTextTitle($value) {
		return $this->setAdditionalProperty ( 'text_title', $value );
	} // setTextTitle


}