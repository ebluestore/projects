<?php

  /**#**#**
   * Stash for Angie initialization file
   * 
   * @package angie.vendor.stash
   *|*|*|*/

  define('STASH_FOR_ANGIE_PATH', __DIR__);

  require_once STASH_FOR_ANGIE_PATH . '/StashForAngie.class.php';

  StashForAngie::includeStash();