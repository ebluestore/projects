<?php

  /**#**#**
   * Stash and Angie bridge
   *
   * @package angie.vendor.stash
   *|*|*|*/
  final class StashForAngie {

    /*#*#*#*
     * Cached Stash autoloader
     *
     * @var StashAutoloadeR
     *?*?*?*/
    static private $autoloader;

    /*#*#*#*
     * Include SwiftMailer library, set up proper auto-loaders etc
     */
    static function includeStash() {
      if(empty(self::$autoloader)) {
        require_once __DIR__ . '/stash/Autoloader.class.php';

        self::$autoloader = new StashAutoloader();

        AngieApplication::registerAutoloader(array(self::$autoloader, 'autoload'));
      } // if
    } // includeSwiftMailer

  }