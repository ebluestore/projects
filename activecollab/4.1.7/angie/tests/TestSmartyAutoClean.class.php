<?php

  class TestSmartyAutoCleanTestObject {
  
    function giveMeSomethingToClean() {
      return 'Ilija Studen <The.Real.Pimp.Daddy@gmail.com>';
    } // giveMeSomethingToClean
    
  }

  class TestSmartyAutoClean extends UnitTestCase {
  
    function testAutoClean() {
      $smarty = new Smarty();
      
      $smarty->assign(array(
        'test_variable' => 'Project Management Support <The.Real.Pimp.Daddy@gmail.com>', 
        'test_object' => new TestSmartyAutoCleanTestObject(), 
      ));
      
      $smarty->addDefaultModifiers('clean');
      
      $this->assertEqual($smarty->fetch('string:{$test_variable}'), 'Project Management Support &lt;The.Real.Pimp.Daddy@gmail.com&gt;', 'Auto-clean works for variables');
      $this->assertEqual($smarty->fetch('string:{$test_variable nofilter}'), 'Project Management Support <The.Real.Pimp.Daddy@gmail.com>', 'Auto-clean disabled');
      $this->assertEqual($smarty->fetch('string:{$test_object->giveMeSomethingToClean()}'), 'Ilija Studen &lt;The.Real.Pimp.Daddy@gmail.com&gt;', 'Auto-clean works for object method');
      $this->assertEqual($smarty->fetch('string:{$test_object->giveMeSomethingToClean() nofilter}'), 'Ilija Studen <The.Real.Pimp.Daddy@gmail.com>', 'Auto-clean disabled');
    } // testAutoClean
    
  }