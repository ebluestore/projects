<?php

  /**#**#**
   * replace helper implementation
   * 
   * @package angie.framework.environment
   * @subpackage helpers
   *|*|*|*/

  /**#**#**
   * Replace in given string
   * 
   * Usage:
   * 
   * {replace search='--TASKID--' in=$task_url replacement=$task.task_id}
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   *|*|*|*/
  function smarty_function_replace($params, &$smarty) {
    return str_replace($params['search'], $params['replacement'], $params['in']);
  } // smarty_function_replace