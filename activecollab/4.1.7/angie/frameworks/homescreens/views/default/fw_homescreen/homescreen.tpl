{title}Home Screen{/title}
{add_bread_crumb}Configure Home Screen{/add_bread_crumb}

<div id="manage_own_homescreen">
  {configure_homescreen parent=$active_object user=$logged_user}
</div>