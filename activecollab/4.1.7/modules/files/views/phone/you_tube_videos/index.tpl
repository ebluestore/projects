{title}All YouTube Videos{/title}
{add_bread_crumb}All YouTube Videos{/add_bread_crumb}

<div id="assets_you_tube_videos">
	<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
		<li data-role="list-divider"><img src="{image_url name="icons/listviews/navigate-icon.png" module=$smarty.const.SYSTEM_MODULE interface=AngieApplication::INTERFACE_PHONE}" class="divider_icon" alt="">{lang}Navigate{/lang}</li>
		{if is_foreachable($you_tube_videos)}
			{foreach $you_tube_videos as $you_tube_video}
				<li><a href="{$you_tube_video->getViewUrl()}">{$you_tube_video->getName()}</a></li>
			{/foreach}
		{else}
	  	<li>{lang}There are no YouTube Videos{/lang}</li>
		{/if}
	</ul>
</div>

<div class="archived_objects">
	<a href="{assemble route=project_assets_you_tube_videos_archive project_slug=$active_project->getSlug()}" data-role="button" data-theme="k">{lang}Archive{/lang}</a>
</div>