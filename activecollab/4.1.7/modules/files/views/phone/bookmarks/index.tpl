{title}All Bookmarks{/title}
{add_bread_crumb}All Bookmarks{/add_bread_crumb}

<div id="assets_bookmarks">
	<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
		<li data-role="list-divider"><img src="{image_url name="icons/listviews/navigate-icon.png" module=$smarty.const.SYSTEM_MODULE interface=AngieApplication::INTERFACE_PHONE}" class="divider_icon" alt="">{lang}Navigate{/lang}</li>
		{if is_foreachable($bookmarks)}
			{foreach $bookmarks as $bookmark}
				<li><a href="{$bookmark->getViewUrl()}">{$bookmark->getName()}</a></li>
			{/foreach}
		{else}
	  	<li>{lang}There are no bookmarks{/lang}</li>
		{/if}
	</ul>
</div>

<div class="archived_objects">
	<a href="{assemble route=project_assets_bookmarks_archive project_slug=$active_project->getSlug()}" data-role="button" data-theme="k">{lang}Archive{/lang}</a>
</div>