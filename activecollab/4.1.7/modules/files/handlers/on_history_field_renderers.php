<?php

/**
 * Handle on_history_field_renderers event
 *
 * @package activeCollab.modules.files
 * @subpackage handlers
 */

/**
 * Get history changes as log text
 *
 * @param $object
 * @param $renderers
 */
function files_handle_on_history_field_renderers(&$object, &$renderers) {
	if ($object instanceof Bookmark) {
		$renderers['varchar_field_1'] = function($old_value, $new_value) {
			if($new_value) {
				if($old_value) {
					return lang('Bookmark URL changed from <b>:old_value</b> to <b>:new_value</b>', array('old_value' => $old_value, 'new_value' => $new_value));
				} else {
					return lang('Bookmark URL set to <b>:new_value</b>', array('new_value' => $new_value));
				} // if
			} else {
				if($old_value) {
					return lang('Bookmark URL set to empty value');
				} // if
			} // if
		};
	} elseif ($object instanceof YouTubeVideo) {
		$renderers['varchar_field_1'] = function($old_value, $new_value) {
			if($new_value) {
				if($old_value) {
					return lang('YouTube Video URL changed from <b>:old_value</b> to <b>:new_value</b>', array('old_value' => $old_value, 'new_value' => $new_value));
				} else {
					return lang('YouTube Video URL set to <b>:new_value</b>', array('new_value' => $new_value));
				} // if
			} else {
				if($old_value) {
					return lang('YouTube Video URL set to empty value');
				} // if
			} // if
		};
	} // if
}