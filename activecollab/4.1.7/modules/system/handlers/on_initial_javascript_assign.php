<?php

  /**#**#**
   * on_initial_javascript_assign event handler
   *
   * @package activeCollab.modules.system
   * @subpackage handlers
   *|*|*|*/

  /**#**#**
   * Populate initial JavaScript variables list
   *
   * @param $variables
   *|*|*|*/
  function system_handle_on_initial_javascript_assign(&$variables) {
  } // system_handle_on_initial_javascript_assign