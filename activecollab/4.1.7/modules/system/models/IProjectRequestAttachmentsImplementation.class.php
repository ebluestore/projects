<?php

  /**#**#**
   * Project request attachments helper implementation
   *
   * @package activeCollab.modules.system
   * @subpackage models
   *|*|*|*/
  class IProjectRequestAttachmentsImplementation extends IAttachmentsImplementation {
    
    /*#*#*#*
     * Create a new attachment instance
     *
     * @return ProjectRequestAttachment
     *7*7*7*/
    function newAttachment() {
      return new ProjectRequestAttachment();
    } // newAttachment
    
  }