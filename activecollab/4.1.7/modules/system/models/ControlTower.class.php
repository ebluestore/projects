<?php

  /**#**#**
   * Application level control tower implementation
   *
   * @package activeCollab.modules.system
   * @subpackage models
   *|*|*|*/
  class ControlTower extends FwControlTower {

    /*#*#*#*
     * Get control tower settings
     *
     * @return array
     *>*>*>*/
    function getSettings() {
      parent::getSettings();

      if ($this->settings === false) {
        $this->settings = array();
      } // if

      $system = lang('System');

      if (!isset($this->settings[$system])) {
        $this->settings[$system] = array();
      } // if

      return $this->settings;
    } // getSettings


  }