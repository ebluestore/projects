<?php

  /**#**#**
   * Client dashboard implementation
   *
   * @package activeCollab.modules.system
   * @subpackage models
   *|*|*|*/
  class ClientDashboard extends UserRoleDashboard {

    /*#*#*#*
     * Return widgets that are displayed to administrators
     *
     * @return HomescreenWidget[]
     */
    function getWidgets() {
      return array(
        HomescreenWidgets::create('WelcomeHomescreenWidget', 1, array(
          'welcome_message' => ConfigOptions::getValue('identity_client_welcome_message'),
          'logo_on_white' => ConfigOptions::getValue('identity_logo_on_white'),
        )),
        HomescreenWidgets::create('AnnouncementsHomescreenWidget', 1),
        HomescreenWidgets::create('SystemNotificationsHomescreenWidget', 1),
        HomescreenWidgets::create('RemindersHomescreenWidget', 1),
        HomescreenWidgets::create('RecentActivitiesHomescreenWidget', 2),
        HomescreenWidgets::create('MyProjectsHomescreenWidget', 3),
      );
    } // getWidgets

  }