/**
 * Render human friendly when to send overdue reminder value
 *
 * @param Number value
 * @return String
 */
App.Wireframe.Utils['formatWhenToSendOverdueReminder'] = function(value) {
  if(!value) {
    return App.lang(':days days', { 'days' : 0 });
  } else if (value == 1) {
    return App.lang('1 day');
  } else if (value == 7) {
    return App.lang('1 week');
  } else if (value == 14) {
    return App.lang('2 weeks');
  } else if (value == 31) {
    return App.lang('1 month');
  } else {
    return App.lang(':days days', { 'days' : value });
  } // if
};

/**
 * Parse human friendly when to send overdue reminder value
 *
 * @param Number value
 * @return Number
 */
App.Wireframe.Utils['parseWhenToSendOverdueReminder'] = function(value) {
  var parsed = App.parseNumeric(value);
  
  // Check whether parsed value is not or it's a decimal number
  return isNaN(parsed) || parsed % 1 != 0 ? 0 : parsed;
};