/**
 * Invoice designer
 *
 * @require App.Wireframe.Events
 */
(function($) {
  
  /**
   * Public methods
   * 
   * @var Object
   *|*|*|*/
  var public_methods = {
      
    /**
     * Initialise context popup
     * 
     * @param Object options
     *      items - items for the list
     *      grouping - grouping cases
     *      filtering - filtering cases
     *      render_item - how to render item
     *      search_index - function to extract search index from item
     * @returns jQuery
     */
    init : function(options) {
      return this.each(function () {
        var wrapper = $(this);
        var wrapper_dom = this;

        this.id_data = {};
                
        this.id_data.settings = jQuery.extend({
          'params'                            : {},
          'maximum_vertical_margin_offset'    : 100,
          'maximum_horizontal_margin_offset'  : 80,
          'guide_grid_size'                   : 5,
          'section_offset'                    : 5
        }, options);

        this.id_data.paper = wrapper.find('.invoice_designer_paper');
  
        this.id_data.settings_buttons = wrapper.find('.invoice_designer_buttons ul');
        
        init_settings_buttons.apply(this);
        init_event_listeners.apply(this);
        render_template.apply(this, [options.template]);
      });
    }
  };
  
  /**
   * Renders the template
   * 
   * @param Obejct template
   * @return null
   *|*|*|*/
  var render_template = function (template) {
    this.id_data.paper.find('.render').remove();
    this.id_data.render = $('<div class="render"></div>').appendTo(this.id_data.paper);
    
    // BACKGROUND IMAGE
    
    if (template.background_image) {
      this.id_data.render.append('<img src="' + template.background_image + '" class="background_image" />');
    } // if
    
    // BASICS
    
    this.id_data.render_header = $('<div class="header"></div>').appendTo(this.id_data.render);
    this.id_data.render_body = $('<div class="body"></div>').appendTo(this.id_data.render);
    this.id_data.render_footer = $('<div class="footer"></div>').appendTo(this.id_data.render);
    
    // HEADER
    
    if (template.print_logo) {
      $('<div class="company_logo"><img src="' + template.company_logo + '" /></div>').appendTo(this.id_data.render_header);
    } // if
    
    if (template.print_company_details) {
      $('<div class="company_details">' + App.clean(template.company_name) + '<br />' + App.clean(template.company_details).nl2br() + '</div>').appendTo(this.id_data.render_header).css('color', template.header_text_color);
    } // if
    
    if (template.print_header_border) {
      this.id_data.render_header.css('border-bottom', '1px solid ' + template.header_border_color);
    } // if
    
    if (parseInt(template.header_layout)) {
      this.id_data.render_header.addClass('inverse_layout');
    } // if
    
    // BODY
    
    this.id_data.render_body_top = $('<div class="body_top"></div>').appendTo(this.id_data.render_body);
    this.id_data.render_invoice_details = $('<div class="invoice_details"></div>').appendTo(this.id_data.render_body_top).css('color', template.invoice_details_text_color);
    this.id_data.render_invoice_details.append('<div class="invoice_number">' + this.id_data.settings.invoice.name['long'] + '</div>');
    this.id_data.render_invoice_details.append('<div class="issued_on">' + App.lang('Issued On: :issue_date', {'issue_date' : this.id_data.settings.invoice.issued_on.formatted_date}) + '</div>');
    this.id_data.render_invoice_details.append('<div class="due_on">' + App.lang('Payment Due On: :due_date', {'due_date' : this.id_data.settings.invoice.due_on.formatted_date}) + '</div>');
    
    this.id_data.render_client_details = $('<div class="client_details"><div class="client_details_inner"><strong>' + this.id_data.settings.invoice.client.name + '</strong><br />' + this.id_data.settings.invoice.client.address.nl2br() + '</div></div>').appendTo(this.id_data.render_body_top).css('color', template.client_details_text_color);
    
    if (parseInt(template.body_layout)) {
      this.id_data.render_body_top.addClass('inverse_layout');
    } // if
    
    // ITEMS
    this.id_data.render_table = $('<table class="items" cellspacing="0"></table>').appendTo(this.id_data.render_body);
    this.id_data.render_table_row = $('<tr></tr>').appendTo(this.id_data.render_table);

    // item row
    if (template['display_item_order']) {
      this.id_data.render_table_row.append('<th>#</th>');
    } // if

    // descriptions
    this.id_data.render_table_row.append('<th>' + App.lang('Description') + '</th>');

    // quantity
    if (template['display_quantity']) {
      this.id_data.render_table_row.append('<th class="quantity">' + App.lang('Qty.') + '</th>');
    } // if

    // unit cost
    if (template['display_unit_cost']) {
      this.id_data.render_table_row.append('<th class="unit_cost">' + App.lang('Unit Cost') + '</th>');
    } // if

    // subtotal
    if (template['display_subtotal']) {
      this.id_data.render_table_row.append('<th class="subtotal">' + App.lang('Subtotal') + '</th>');
    } // if

    // tax
    if (template['display_tax_amount'] || template['display_tax_rate']) {
      this.id_data.render_table_row.append('<th class="tax">' + App.lang('Tax') + '</th>');
    } // if

    // total
    if (template['display_total']) {
      this.id_data.render_table_row.append('<th class="total">' + App.lang('Total') + '</th>');
    } // if
    
    var designer = this;
    var counter = 1;
    $.each(this.id_data.settings.invoice.items, function (item) {
      var item_row = $('<tr></tr>').appendTo(designer.id_data.render_table);

      if (template['display_item_order']) {
        item_row.append('<td class="number">' + counter + '.</td>');
      } // if

      // descriptions
      item_row.append('<td class="description">' + this.description + '</td>');

      // quantity
      if (template['display_quantity']) {
        item_row.append('<td class="quantity">' + App.numberFormat(this['quantity']) + '</td>');
      } // if

      // unit cost
      if (template['display_unit_cost']) {
        item_row.append('<td class="unit_cost">' + App.numberFormat(this['unit_cost']) + '</td>');
      } // if

      // subtotal
      if (template['display_subtotal']) {
        item_row.append('<td class="subtotal">' + App.numberFormat(this['subtotal']) + '</td>');
      } // if

      // tax
      if (template['display_tax_amount']) {
        item_row.append('<td class="tax">' + App.numberFormat(this['first_tax']['value']) + '</td>');
      } else if (template['display_tax_rate']) {
        item_row.append('<td class="tax">' + (this['first_tax']['verbose_percentage'] ? this['first_tax']['verbose_percentage'] : '-') + '</td>');
      } // if

      // total
      if (template['display_total']) {
        item_row.append('<td class="total">' + App.numberFormat(this['total']) + '</td>');
      } // if

      counter++;
    });
        
    this.id_data.render_items_total = $('<table class="items_total" cellspacing="0"></table>').insertAfter(this.id_data.render_table);
    this.id_data.render_items_total.append('<tr><td class="label">' + App.lang('Subtotal') + '</td><td class="amount">' + this.id_data.settings.invoice.subtotal.toFixed(2) + '</td></tr>');
    this.id_data.render_items_total.append('<tr><td class="label">' + App.lang('Tax Total') + '</td><td class="amount">' + this.id_data.settings.invoice.tax.toFixed(2) + '</td></tr>');
    this.id_data.render_items_total.append('<tr><td class="label">' + App.lang('Total') + '</td><td class="amount">' + this.id_data.settings.invoice.total.toFixed(2) + '</td></tr>');
    
    this.id_data.render_table.css('color', template.items_text_color);
    this.id_data.render_items_total.css('color', template.items_text_color);
    
    if (template.print_items_border) {
      this.id_data.render_table.find('td,th').css('border-bottom', '1px solid ' + template.items_border_color);
      this.id_data.render_items_total.find('td').css('border-bottom', '1px solid ' + template.items_border_color);      
    } // if
    
    if (template.print_table_border) {
      this.id_data.render_table.find('th').css('border-bottom', '1px solid ' + template.table_border_color);
      this.id_data.render_table.find('tr:last-child td').css('border-bottom', '1px solid ' + template.table_border_color);
      this.id_data.render_items_total.find('tr:last-child td').css('border-bottom', '1px solid ' + template.table_border_color);
    } // if
    
    // NOTE
    this.id_data.render_note = $('<div class="invoice_note"><strong>' + App.lang('Note') + ':</strong><br />' + this.id_data.settings.invoice.note + '</div>').appendTo(this.id_data.render_body);
    this.id_data.render_note.css('color', template.note_text_color);
    
    // FOOTER

    if (template.print_footer) {
      this.id_data.render_footer.css('color', template.footer_text_color);
      this.id_data.render_footer.append('<div class="invoice_number">' + this.id_data.settings.invoice.name['long'] + '</div>');
      this.id_data.render_footer.append('<div class="pager">' + App.lang('Page 1 of 1') + '</div>');

      if (template.print_footer_border) {
        this.id_data.render_footer.css('border-top', '1px solid ' + template.footer_border_color);
      } // if

      if (parseInt(template.footer_layout)) {
        this.id_data.render_footer.addClass('inverse_layout')
      } // if
    } // if
    
  }; // render_template
  
  /**
   * Event listeners
   * 
   * @param void
   * @return null
   *|*|*|*/
  var init_event_listeners = function () {
    var designer = this;
    App.Wireframe.Events.bind('invoice_settings_updated.content', function (event, invoice_template) {
      render_template.apply(designer, [invoice_template]);
    });
  }; // init_event_listeners
  
  /**
   * Initialize settings buttons
   * 
   * @param void
   * @return null
   *|*|*|*/
  var init_settings_buttons = function () {
    this.id_data.settings_buttons.find('a').each(function () {
      var anchor = $(this);
      
      anchor.flyoutForm({
        'title'         : anchor.text(),
        'success_event' : 'invoice_settings_updated',
        'width'         : anchor.attr('flyout_width') ? anchor.attr('flyout_width') : undefined
      });
    });
  }; // init_settings_buttons
  
  ////////////////////////////////////////////////////////// PLUGIN INITIAL DATA ///////////////////////////////////////////////////////////  
  
  /**
   * Plugin name
   * 
   * @var String
   *|*|*|*/
  var plugin_name = 'invoiceDesigner';
        
  ///////////////////////////////////////////////////////// PLUGIN FRAMEWORK STUFF /////////////////////////////////////////////////////////
  /**
   * Register jQuery plugin and handle public methods
   * 
   * @param mixed method
   * @return null
   *|*|*|*/
  $.fn[plugin_name] = function(method) { 
    if (public_methods[method]) {
      return public_methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
      return public_methods.init.apply(this, arguments);
    } else {
      $.error('Method ' +  method + ' does not exist in jQuery.' + plugin_name);
    } // if
  };

})(jQuery);