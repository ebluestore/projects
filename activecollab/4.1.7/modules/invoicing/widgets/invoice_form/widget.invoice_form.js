/**
 * Invoice form widget
 * 
 * @param void
 * @return null
 */
(function($) {
  /**
   * Plugin name
   * 
   * @var String
   *|*|*|*/
  var plugin_name = 'invoiceForm';
      
  /**
   * Default settings
   * 
   * @var Object
   *|*|*|*/
  var settings = {
    mode                  : 'add',
    notes                 : {},
    initial_note          : '',
    item_templates        : {},
    company_details_url   : '',
    company_projects_url  : '',
    move_icon_url         : ''
  };
  
  /**
   * Public methods
   * 
   * @var Object
   *|*|*|*/
  var public_methods = {
    /**
     * Initialise invoice form
     * 
     * @param Object options
     * @returns jQuery
     */
    init : function(options) {
      return this.each(function () {

        var _this = this;
        var $this = $(this);
        
        _initvar($this);  
        _var($this).settings = $.extend({}, settings, options || {});
        
        initialize_id_field.apply(_this);
        initialize_notes_chooser.apply(_this);
        initialize_company_chooser.apply(_this);
        initialize_items_editor.apply(_this);
        initialize_project_picker.apply(_this);
        initialize_currency_picker.apply(_this);
        initialize_compound_toggler.apply(_this);

        validate_form_elements.apply(_this);
      });
    }
  };
  
  /**
   * Validate various form elements
   *|*|*|*/
  var validate_form_elements = function () {
    var wrapper = $(this);
    
    var occurrence = wrapper.find('input[type="text"].occurrence:first');
    if (occurrence.length) {
      occurrence.bind('change blur', function (event) {
        var value = occurrence.val();
        if (parseInt(value) != value || value < 1) {
          occurrence.val(1);
        } // if
      });
    } // if
  }; // validate_form_elements
  
  /**
   * Initialize items editor
   * 
   * @param void
   * @return null
   *|*|*|*/
  var initialize_items_editor = function () {
    var _this = this;
    var $this = $(this);
    
    var items_table = _var($this).items_table = $this.find('div.invoice_items_wrapper table:first');
    var items_table_body = _var($this).items_table_body = items_table.find('tbody:first');
    var main_form_column = _var($this).main_form_column = $this.find('.main_form_column');

    if (_var($this).settings.second_tax_is_enabled) {
      items_table.addClass('two_taxes');
    } // if

    // create tax_rate_chooser
    var tax_rates = _var($this).settings.tax_rates;
    var tax_rate_chooser = $('<select><option value="0" rate="0">' + App.lang('No Tax') + '</option></select>');
    
    var tax_rate_chooser_select = tax_rate_chooser;
    if(tax_rates) {
	    $.each(tax_rates, function () {
	    	tax_rate_chooser_select.append('<option rate="' + this.percentage + '" value="' + this.id + '">' + this.name + '</option>');
	    });  
    }//if
    _var($this).tax_rate_chooser = tax_rate_chooser;
    
    // click on the 'Add New Item' button
    var add_item_button = _var($this).add_item_button = $this.find('div.invoice_items_wrapper #add_new:first');
    add_item_button.click(function () {
      add_item_row.apply(_this);
      return false;
    });
    
    handle_keypresses.apply(_this);
    handle_input_changes.apply(_this);
    handle_deletion.apply(_this);
    handle_item_templates.apply(_this);
    handle_reordering.apply(_this);
    handle_cells.apply(_this);
    
    var items = _var($this).settings.items;
    
    // if we have pre-existing items, populate the form
    if (items && items.length) {
      $.each(items, function (id, data) {
        add_item_row.apply(_this, [{
          id	: data.id,
          description : data.description,
          unit_cost : data.unit_cost,
          quantity : data.quantity,
          first_tax_rate_id : data.first_tax_rate_id,
          second_tax_rate_id : data.second_tax_rate_id
        }, true]);
      });

      // reindex and recalculate
      recalculate_invoice_totals.apply(_this, [true]);
      reindex_item_rows.apply(_this)
      scroll_to_bottom.apply(_this, [true]);
    } else {
      add_item_row.apply(_this);
      reindex_item_rows.apply(_this);
    } // if

    setTimeout(function () {
      items_table_body.find('tr:last-child textarea').focus();
    }, 50);
  }; // initialize_items_editor

  /**
   * Handle item templates
   *
   * @return null
   *|*|*|*/
  var handle_item_templates = function () {
    var _this = this;
    var $this = $(this);
    
    var add_from_template_button = $this.find('div.invoice_items_wrapper #add_from_template:first');
    var item_templates = _var($this).settings.item_templates;
      
//    TODO: add from template
//    add_from_template_button.click(function () {      
//      return false;
//    });
    
    add_from_template_button.find('a').each(function () {
      var anchor = $(this);
      var template_description = anchor.text();

      anchor.text(App.excerpt(template_description, 27));

      anchor.click(function () {
        var current_template = item_templates[anchor.attr('href')];
        
        add_item_row.apply(_this, [{
          description : current_template.description,
          unit_cost : current_template.unit_cost,
          quantity : current_template.quantity,
          first_tax_rate_id : current_template.first_tax_rate_id,
          second_tax_rate_id : current_template.second_tax_rate_id
        }]);
        return false;
      });
    });
    
  }; // handle_item_templates
  
  /**
   * Handle item deletion
   * 
   * @param void
   * @return null
   *|*|*|*/
  var handle_deletion = function () {
    var _this = this;
    var $this = $(this);
    
    var items_table = _var($this).items_table;
    items_table.click(function (event) {
      var invoker = event.target;
      var $invoker = $(invoker);
      
      if ($invoker.is('img.button_remove')) {
        delete_item_row.apply($this, [$invoker.parents('tr:first')]);
        return false;
      } // if
    });
  }; // handle_deletion

  var resize_textarea = function (textarea, add_newline) {
    if (!textarea.textarea) {
      textarea.textarea = $(textarea);
    } // if

    var line_height = 15;

    textarea.textarea.scrollTop(0);
    textarea.textarea.height(line_height);

    var scroll_height = $.browser.webkit ? textarea.textarea.prop('scrollHeight') - 6 : textarea.textarea.prop('scrollHeight');
    textarea.textarea.height(scroll_height + (add_newline ? line_height : 0));
  } // resize_textarea
  
  /**
   * Handle all keyboard interaction with items editor
   * 
   * @param void
   * @return null
   *|*|*|*/
  var handle_keypresses = function () {
    var _this = this;
    var $this = $(this);
    var items_table = _var($this).items_table;

    items_table.on('keydown', 'input', function (event) {
      var invoker = event.target;

      if (event.which == 13) {
        return false;
      } // if

      if (new RegExp('\\bnumber_input\\b').test(invoker.className)) {
        if ((event.which >= 48) && (event.which <= 57) || (event.which == 46) || (event.which == 45) || (event.which == 43) || (event.altKey || event.metaKey || event.ctrlKey || (event.keyCode > 0))) {
          // check if dot is already present
          if ((event.which == 46) && (invoker['value'].indexOf('.') > 0)) {
            return false;
          } else {
            return true;
          } // if
        } else {
          return false;
        } // if
      } // if
      return true;
    }).on('keydown', 'textarea', function (event) {
      resize_textarea(this, event.which == 13);
    }).on('keyup', 'textarea', function () {
      resize_textarea(this);
    }).on('change', 'textarea', function () {
      resize_textarea(this);
    });

//  if ((event.which == 13) && (invoker_class == 'INPUT')) {
//    $invoker = $(invoker);
//    $invoker.change();
//
//    var next_row = $invoker.parents('tr:first').next();
//    if (next_row.is('tr')) {
//      next_row.find('input[type=text], select').eq(0).focus();
//    } else {
//      add_item_row.apply(_this);
//    } // if
//    return false;
  }; // handle_keypresses
  
  /**
   * Handle changes of certain inputs
   * 
   * @param void
   * @return null
   *|*|*|*/
  var handle_input_changes = function () {
    var _this = this;
    var $this = $(this);
    
    var items_table = _var($this).items_table;

    items_table.bind('change', function (event) {
      var invoker = event.target;
      var invoker_class = invoker.tagName.toUpperCase();

      if (invoker_class == 'INPUT' || invoker_class == 'SELECT') {
        $invoker = $(invoker);
        var invoker_row = $invoker.parents('tr:first');
        if ($invoker.is('.number_input')) {
          $invoker.val(App.parseNumeric($invoker.val()).toFixed(get_number_of_decimal_spaces.apply(_this)));
        } // if
        recalculate_item_row.apply(_this, [invoker_row, $invoker]);
      } // if
    });
    
    items_table.bind('click', function (event) {
      var invoker = event.target;
      var invoker_class = invoker.tagName.toUpperCase();
      
      if (invoker_class == 'SELECT') {
        $invoker = $(invoker);
        var invoker_row = $invoker.parents('tr:first');
        recalculate_item_row.apply(_this, [invoker_row]);
      } // if      
    });
  }; // handle_input_changes

  /**
   * Update items dropdown position
   *|*|*|*/
  var update_items_dropdown_position = function () {
    var _this = this;
    var $this = $(this);

    if (!_var($this).items_table_wrapper) {
      _var($this).items_table_wrapper = $this.find('.invoice_items_wrapper:first');
    } // if

    if (!_var($this).main_invoice_settings) {
      _var($this).main_invoice_settings = $this.find('.main_invoice_settings:first');
    } // if

    if ((_var($this).main_form_column.height() - 70) < (_var($this).items_table_wrapper.height() + _var($this).main_invoice_settings.height())) {
      _var($this).main_form_column.addClass('popup_on_top');
    } else {
      _var($this).main_form_column.removeClass('popup_on_top');
    } // if
  }; // update_items_dropdown_position

  var handle_cells = function () {
    var _this = this;
    var $this = $(this);
    var items_table = _var($this).items_table;

    items_table.on('blur', 'input[type="number"], input[type="text"], select, textarea', function () {
      var input_field = $(this);

      if (!this.parent_cell) {
        this.parent_cell = input_field.parents('td:first');
      } // if

      this.parent_cell.removeClass('focused');
    }).on('focus', 'input[type="number"], input[type="text"], select, textarea', function () {
      var input_field = $(this);

      if (!this.parent_cell) {
        this.parent_cell = input_field.parents('td:first');
      } // if

      this.parent_cell.addClass('focused');
    }).on('click', 'td.field_cell', function () {
      var cell = $(this);

      if (!this.field) {
        this.field = cell.find('input:first, select:first');
      } // if

      this.field.focus();
    });
  }
  
  /**
   * Add an row to the items editor table
   * 
   * @param void
   * @return null
   *|*|*|*/
  var add_item_row = function (values, batch_add) {
    var _this = this;
    var $this = $(this);
    
    var items_table_body = _var($this).items_table_body;
    var one = 1;
    var zero = 0;

    var rows = items_table_body.find('tr');

    if (!batch_add) {
      if (rows.length) {
        var destination_row = null
        rows.each(function () {
          var row = $(this);
          var row_description = row.find('td.description textarea:first');
          var row_quantity = row.find('td.quantity').find('input[type=text], input[type=number]').eq(0);
          var row_unit_cost = row.find('td.unit_cost').find('input[type=text], input[type=number]').eq(0);
          var row_unit_total = row.find('td.total').find('input[type=text], input[type=number]').eq(0);

          if (!row_description.val() && !parseInt(row_unit_cost.val()) && !parseInt(row_unit_total.val()) && (row_quantity.val() == one.toFixed(get_number_of_decimal_spaces.apply(_this)))) {
            destination_row = row;
            return false;
          } // if
        });

        if (destination_row) {
          if (values) {
            do_update_item_row.apply(_this, [destination_row, values]);
          } else {
            destination_row.find('input[type="text"]:first').focus();
          } // if
        } else {
          do_add_item_row.apply(_this, [values]);
        } // if
      } else {
        do_add_item_row.apply(_this, [values]);
      } // if
      
      recalculate_invoice_totals.apply(_this);
      reindex_item_rows.apply(_this);
      scroll_to_bottom.apply(_this);
    } else {
      do_add_item_row.apply(_this, [values]);
    } // if
  }; // add_item_row
  
  /**
   * Add the item to the end of the list
   * 
   * @param Object values
   * @return null
   *|*|*|*/
  var do_add_item_row = function (values) {
    var _this = this;
    var $this = $(this);
    
    var items_table_body = _var($this).items_table_body;
    var zero = 0;
    var one = 1;
    
    // determine next row number
    var row_number = 0;
    items_table_body.find('tr.item').each(function () {
      var loop_row_id = $(this).attr('row_number');
      row_number = Math.max(loop_row_id, row_number);
    });
    row_number++;

    var decimal_spaces = get_number_of_decimal_spaces.apply(_this);
    var next_row_name = 'invoice[items][' + row_number + ']';
    var row = $('<tr class="item" id="items_row_' + row_number + '" row_number="' + row_number + '">' +
      '<td class="num"><span></span><img src="' + _var($this).settings.move_icon_url + '" class="move_handle" /><input type="hidden" name="' + next_row_name + '[id]" value="' + (values && values.id ? values.id : 0) + '"/></td>' +
      '<td class="description field_cell"><textarea name="' + next_row_name + '[description]" required="true" spellcheck="false" resize="none">' + (values && values.description ? values.description : '') + '</textarea></td>' +
      '<td class="quantity field_cell"><input type="number" name="' + next_row_name + '[quantity]" class="short number_input input_quantity" step="0.01" value="' + (values && values.quantity ? values.quantity.toFixed(decimal_spaces) : one.toFixed(decimal_spaces)) + '" /></td>' +
      '<td class="unit_cost field_cell"><input type="number" name="' + next_row_name + '[unit_cost]" class="short number_input input_unit_cost" step="0.01" value="' + (values && values.unit_cost ? values.unit_cost.toFixed(decimal_spaces) : zero.toFixed(decimal_spaces)) + '" /></td>' +
      '<td class="subtotal field_cell" style="display: none"><input type="hidden" name="' + next_row_name + '[subtotal]" value="" /></td>' +
      '<td class="total field_cell"><input type="number" name="invoice' + next_row_name + '[total]" value="'+zero.toFixed(decimal_spaces)+'" class="number_input input_total" step="0.01"/></td>' +
      '<td class="options"><img src="' + _var($this).settings.delete_icon_url + '" class="button_remove" /></td>' + 
    '</tr>');

    var unit_cost = row.find('td.unit_cost');

    if (_var($this).settings.second_tax_is_enabled) {
      $('<td class="tax_rate second_tax_rate field_cell dropdown_cell"></td>').append(_var($this).tax_rate_chooser.clone().attr('name', next_row_name+'[second_tax_rate_id]').attr('value', (values && values.second_tax_rate_id ? values.second_tax_rate_id : ''))).insertAfter(unit_cost);
      $('<td class="tax_rate first_tax_rate field_cell dropdown_cell"></td>').append(_var($this).tax_rate_chooser.clone().attr('name', next_row_name+'[first_tax_rate_id]').attr('value', (values && values.first_tax_rate_id ? values.first_tax_rate_id : ''))).insertAfter(unit_cost);
    } else {
      $('<td class="tax_rate first_tax_rate field_cell dropdown_cell"></td>').append(_var($this).tax_rate_chooser.clone().attr('name', next_row_name+'[first_tax_rate_id]').attr('value', (values && values.first_tax_rate_id ? values.first_tax_rate_id : ''))).insertAfter(unit_cost);
    } // if

    row.appendTo(items_table_body);

    resize_textarea(row.find('textarea').get(0));

    if (!values && _var($this).settings.default_tax_rate) {
      row.find('td.tax_rate select').val(_var($this).settings.default_tax_rate['id']);
    } // if
        
    row.find('textarea:first').focus();

    recalculate_item_row.apply(_this, [row]);

    update_items_dropdown_position.apply(this);
  }; // do_add_item_row
  
  /**
   * Update item row with values
   * 
   * @param jQuery row
   * @param Object values
   * @return null
   *|*|*|*/
  var do_update_item_row = function (row, values) {
    var _this = this;
    var $this = $(this);
    
    var zero = 0;
    var one = 0;

    var decimal_spaces = get_number_of_decimal_spaces.apply(_this);
    
    row.find('td.description textarea:first').attr('value', values && values.description ? values.description : '').focus();
    row.find('td.unit_cost input:first').attr('value', values && values.unit_cost ? values.unit_cost.toFixed(decimal_spaces) : one.toFixed(decimal_spaces));
    row.find('td.quantity input:first').attr('value', values && values.quantity ? values.quantity.toFixed(decimal_spaces) : zero.toFixed(decimal_spaces));

    var first_tax_rate = row.find('td.tax_rate.first_tax_rate select:first').attr('value', values && values.first_tax_rate_id ? values.first_tax_rate_id : '');
    if (values && !values.first_tax_rate_id) {
      first_tax_rate.find('option:first').attr('selected', 'selected');
    } // if

    if (_var($this).settings.second_tax_is_enabled) {
      var second_tax_rate = row.find('td.tax_rate.second_tax_rate select:first').attr('value', values && values.second_tax_rate_id ? values.second_tax_rate_id : '');
      if (values && !values.second_tax_rate_id) {
        second_tax_rate.find('option:first').attr('selected', 'selected');
      } // if
    } // if

    resize_textarea(row.find('textarea').get(0));

    recalculate_item_row.apply(_this, [row]);
  }; // do_update_item_row
  
  /**
   * Delete the item row
   * 
   * @param jQuery row
   * @return null
   *|*|*|*/
  var delete_item_row = function (row) {
    var _this = this;
    var $this = $(this);
    
    var prev_row = row.prev('tr');
    var next_row = row.next('tr');
    
    // remove the row only if it's not the last one
    if ((prev_row.length > 0) || (next_row.length > 0)) {
      row.remove();

      if (prev_row.length > 0) {
        prev_row.find('textarea, select, input[type=text]').eq(0).focus();
      } else if (next_row.length > 0) {
        next_row.find('textarea, select, input[type=text]').eq(0).focus();
      } // if
      
      recalculate_invoice_totals.apply(_this);
      reindex_item_rows.apply(_this);
    } // if

    update_items_dropdown_position.apply(this);
  }; // delete_item_row
  
  /**
   * Recalculate item row
   * 
   * @param jQuery row
   * @return null
   *|*|*|*/
  var recalculate_item_row = function (row, invoker) {
    var _this = this;
    var $this = $(this);
    
    var total_field = row.find('td.total input:first');
    var unit_cost_field = row.find('td.unit_cost input:first');
    var subtotal_field = row.find('td.subtotal input:first');

    var decimal_spaces = get_number_of_decimal_spaces.apply(_this);
    
    var total = App.parseNumeric(total_field.val()).toFixed(decimal_spaces);
    var unit_cost = App.parseNumeric(unit_cost_field.val()).toFixed(decimal_spaces);
    var quantity = App.parseNumeric(row.find('td.quantity input').val()).toFixed(decimal_spaces);
    var first_tax_rate = App.parseNumeric(row.find('td.first_tax_rate select option:selected').attr('rate'));
    var zero = 0;

    if (_var($this).settings.second_tax_is_enabled) {
      var second_tax_rate = App.parseNumeric(row.find('td.second_tax_rate select option:selected').attr('rate'));
    } // if

    if (invoker && invoker.is('.input_quantity') && invoker.val() < 0) {
      invoker.val(zero.toFixed(decimal_spaces));
    } // if

    if (invoker && invoker.is('.input_total')) {
      // we will recalculate unit cost
      if(isNaN(quantity) || isNaN(unit_cost)) {
        var subtotal = 0; var unit_cost = 0;
      } else {
        var subtotal = quantity * unit_cost;
        if (_var($this).settings.second_tax_is_enabled) {
          if (_var($this).settings.second_tax_is_compound) {
            var unit_cost = total / ((first_tax_rate / 100 + 1) * (second_tax_rate / 100 + 1) * quantity);
          } else {
            var unit_cost = total / (quantity * (1 + first_tax_rate/100 + second_tax_rate/100));
          } // if
        } else {
          var unit_cost = total / (quantity * (1 + first_tax_rate/100));
        } // if
      } // if
      unit_cost_field.val(unit_cost.toFixed(decimal_spaces));

    } else {
      // we will recalculate total cost
      if(isNaN(quantity) || isNaN(unit_cost)) {
        var subtotal = 0; var total = 0;
      } else {
        var subtotal = quantity * unit_cost;
        if (_var($this).settings.second_tax_is_enabled) {
          if (_var($this).settings.second_tax_is_compound) {
            var first_tax = subtotal * first_tax_rate/100;
            var second_tax = (first_tax + subtotal) * second_tax_rate/100;
            var total = subtotal + first_tax + second_tax;
          } else {
            var total = subtotal * (1 + first_tax_rate/100 + second_tax_rate/100);
          } // if
        } else {
          var total = subtotal * (1 + first_tax_rate/100);
        } // if
      } // if
      total_field.val(total.toFixed(decimal_spaces));
    } // if

    subtotal_field.val(subtotal.toFixed(decimal_spaces));
    
    recalculate_invoice_totals.apply(_this);
  }; // recalculate_item_row
  
  /**
   * Recalculate invoice totals
   * 
   * @param void
   * @return null
   *|*|*|*/
  var recalculate_invoice_totals = function (full_recalculation) {
    var _this = this;
    var $this = $(this);
    
    var items_table = _var($this).items_table;
    var items_table_body = _var($this).items_table_body;
    
    if (full_recalculation) {
      items_table_body.find('tr').each(function () {
        recalculate_item_row.apply(_this, [$(this)]);
      });
    } // if
    
    var subtotal = 0;
    var total = 0;

    // recalculate total subtotal
    items_table_body.find('tr.item td.subtotal input').each(function() {
      subtotal += App.parseNumeric($(this).val());
    });
    
    // recalculate total of totals
    items_table_body.find('tr.item td.total input').each(function() {
      total += App.parseNumeric($(this).val());
    });

    var decimal_spaces = get_number_of_decimal_spaces.apply(_this);
    
    items_table.find('tfoot tr.invoice_subtotal td.total input:first').val(subtotal.toFixed(decimal_spaces));
    items_table.find('tfoot tr.invoice_total td.total input:first').val(total.toFixed(decimal_spaces));
  }; // recalculate_invoice_totals
  
  /**
   * Reindex item rows
   * 
   * @param void
   * @return null
   *|*|*|*/
  var reindex_item_rows = function () {
    var _this = this;
    var $this = $(this);
    
    var items_table = _var($this).items_table;
    
    var counter = 1;
    items_table.find('tr.item td.num span').each(function() {
      this.innerHTML = counter;
      counter++;
    });
  }; // reindex_item_rows

  /**
   * Scroll to bottom
   *
   * @param with_delay
   *|*|*|*/
  var scroll_to_bottom = function (with_delay) {
    var _this = this;
    var $this = $(this);

    if (with_delay) {
      setTimeout(function () {
        _var($this).main_form_column.stop().scrollTo({'top' : _var($this).items_table_body.height()}, {'duration' : 1000});
      }, 500);
    } else {
      _var($this).main_form_column.stop().scrollTo({'top' : _var($this).items_table_body.height()}, {'duration' : 1000});
    } // if
  }; // scrollToBottom
  
  /**
   * handle item reording
   * 
   * @param void
   * @return null
   *|*|*|*/
  var handle_reordering = function () {
    var _this = this;
    var $this = $(this);
    
    var items_table_body = _var($this).items_table_body;
    
    items_table_body.sortable({
      axis : 'y',
      items : 'tr.item',
      handle : '.move_handle',
      start : function () {
        
      },
      stop : function () {
        reindex_item_rows.apply(_this);
      }
    });
  }; // handle_reordering
  
  /**
   * Initialize ID field
   * 
   * @param void
   * @return null
   *|*|*|*/
  var initialize_id_field = function () {
    var _this = this;
    var $this = $(this);
    
    var ID_autogenerate = $this.find('#autogenerateID');
    var ID_manually = $this.find('#manuallyID');
    
    ID_autogenerate.find('a').click(function () {
      ID_autogenerate.hide();
      ID_manually.show();
      return false;
    });
    
    ID_manually.find('a').click(function () {
      ID_autogenerate.show();
      ID_manually.hide();
      ID_manually.find('input').val('');
      return false;
    });       
  }; // initialize_id_field
  
  /**
   * Initialize notes chooser
   * 
   * @param void
   * @return null
   *|*|*|*/
  var initialize_notes_chooser = function () {

    // obsolete since there is helper only for this purpose on the _invoice_form
    return true;
    
    var _this = this;
    var $this = $(this);
    
    var select_predefined = $this.find('#predefined_notes');
    var note_field = $this.find('#invoice_note');
    
    select_predefined.change(function () {
      var selected_id = select_predefined.attr('value');
      if (_var($this).settings.notes && _var($this).settings.notes[selected_id] !== undefined) {
        note_field.val(_var($this).settings.notes[selected_id]);
        note_field.show();
      } else if (selected_id == 'empty') {
        note_field.val('');
        note_field.hide();
      } else if (selected_id == 'original') {
        note_field.val(_var($this).settings.initial_note);
        note_field.show();
      } else if (selected_id == 'custom') {
        note_field.val('');
        note_field.show();
      } // if
    });
    
    select_predefined.change();    
  }; // initialize_notes_chooser
  
  /**
   * Initialize notes chooser
   * 
   * @param void
   * @return null
   *|*|*|*/
  var initialize_company_chooser = function () {
    var _this = this;
    var $this = $(this);
    
    if (_var($this).settings.company_details_url) {
      var company_id = $this.find('#companyId');
      var company_address = $this.find('#companyAddress');
      
      var ajax_request;    
      company_id.change(function () {
        if (company_id.val()) {
          var ajax_url = App.extendUrl(_var($this).settings.company_details_url, {
            'company_id' : company_id.val(),
            'skip_layout' : 1
          });

          // abort request if already exists and it's active
          if ((ajax_request) && (ajax_request.readyState !=4)) {
            ajax_request.abort();
          } // if

          if (!company_address.is('loading')) {
            company_address.addClass('loading');
          } // if

          company_address.attr("disabled", true);
          company_id.attr("disabled", true);

          ajax_request = $.ajax({
            url         : ajax_url,
            success     : function (response) {
              company_address.val(response ? response : '');
              company_address.removeClass('loading');
              company_address.attr("disabled", false);
              company_id.attr("disabled", false);
            }
          });
        } // if
      });
      
      if (_var($this).settings.mode == 'add') {
        company_id.change();
      } // if
    } // if
  }; // initialize_company_chooser

  /**
   * Initialize showing only those projects where the selected company is set as client
   *|*|*|*/
  var initialize_project_picker = function() {
    var _this = this;
    var $this = $(this);

    if (_var($this).settings.company_projects_url) {
      var projects_picker = $this.find('select.select_project');
      var company_picker = $this.find('#companyId');
      var filter = $this.find('#selected_company_projects');
      var active_projects_wrapper = $('#opt_active');
      var completed_projects_wrapper = $('#opt_completed');
      var projects_list = {
        'active' : [],
        'completed' : []
      };

      // no projects
      if (projects_picker.length == 0) {
        filter.parent().remove();
        return;
      } // if

      // collect all projects
      active_projects_wrapper.find('option').each(function () {
        projects_list.active[$(this).val()] = $(this).text();
      });
      completed_projects_wrapper.find('option').each(function () {
        projects_list.completed[$(this).val()] = $(this).text();
      });

      // toggle checkbox
      filter.click(function() {
        filter_projects();
      });

      // filter when company is changed
      company_picker.change(function() {
        filter_projects();
      });

      // perform filtering or show all projects
      var filter_projects = function() {
        if (filter.is(':checked')) {
          do_filter_projects();
        } else {
          show_all_projects();
        } // if
      };

      // actual filtering
      var do_filter_projects = function() {
        var company_project_ids = get_project_ids();
        if (company_project_ids.active.length == 0 && company_project_ids.completed == 0) {
          hide_all_projects();
          App.Wireframe.Flash.error('Selected company is not client on any of existing projects');
        } else {
          feed_project_list(company_project_ids);
        } // if
      };

      // get project id's that selected company is assigned to as a client
      var get_project_ids = function() {
        var project_ids;
        var company_projects_url = App.extendUrl(_var($this).settings.company_projects_url.replace('--COMPANY_ID--', company_picker.val()), {
          'for_select_box' : 1
        });

        company_picker.attr('disabled', true);
        projects_picker.attr('disabled', true);

        $.ajax({
          url : company_projects_url,
          async : false,
          success : function (response) {
            project_ids = response;
            company_picker.attr('disabled', false);
            projects_picker.attr('disabled', false);
          }
        });

        return project_ids;
      };

      // show all projects
      var show_all_projects = function() {
        feed_project_list(projects_list);
      };

      // hide all projects
      var hide_all_projects = function() {
        active_projects_wrapper.html('');
        completed_projects_wrapper.html('');
      };

      // feed the select with specified projects
      var feed_project_list = function(projects)  {
        hide_all_projects();

        $.each(projects.active, function(id, name) {
          if (typeof name !== 'undefined') {
            active_projects_wrapper.append(
              '<option value="' + id + '">' + name + '</option>'
            );
          } // if
        });

        $.each(projects.completed, function(id, name) {
          if (typeof name !== 'undefined') {
            completed_projects_wrapper.append(
              '<option value="' + id + '">' + name + '</option>'
            );
          } // if
        });
      };
    } // if
  }; // initialize_project_picker

  /**
   * Initialize currency picker
   *|*|*|*/
  var initialize_currency_picker = function () {
    var wrapper = $(this);
    var picker = wrapper.find('#currencyId');

    picker.change(function () {
      var currency = _var(wrapper).settings.currencies[picker.val()];
      var decimal_spaces = currency['decimal_spaces'];
      var step = '0.01';

      if (decimal_spaces) {
        step = Math.pow(0.1, decimal_spaces).toFixed(decimal_spaces);
      } else {
        step = '1';
      } // if

      wrapper.find('div.main_form_column input[type="number"]').each(function () {
        var field = $(this);
        field.attr('step', step);

        var value = parseFloat(field.val());
        field.val(value.toFixed(decimal_spaces));
      });
    });

    picker.change();
  }; // initialize_currency_picker

  /**
   * Initialize Compound Toggler
   *|*|*|*/
  var initialize_compound_toggler = function () {
    var wrapper = $(this);
    var _this = this;
    var toggler = wrapper.find('#second_tax_is_compound_toggler');

    toggler.click(function () {
      _var(wrapper).settings.second_tax_is_compound = $(this).is(':checked');
      recalculate_invoice_totals.apply(_this, [true]);
    });
  } // initialize_compound_toggler

  /**
   * Get Number of decimal spaces
   *
   * @return Number
   *|*|*|*/
  var get_number_of_decimal_spaces = function () {
    var wrapper = $(this);
    var number_of_decimal_spaces = 2;
    var picker = wrapper.find('#currencyId');

    var currency = _var(wrapper).settings.currencies[picker.val()];
    if (currency) {
      return currency['decimal_spaces'];
    } // if

    return number_of_decimal_spaces;
  }; // get_number_of_decimal_spaces
  
  ///////////////////////////////////////////////////////// PLUGIN FRAMEWORK STUFF /////////////////////////////////////////////////////////
  /**
   * Register jQuery plugin and handle public methods
   * 
   * @param mixed method
   * @return null
   *|*|*|*/
  $.fn[plugin_name] = function(method) {    
    if (public_methods[method]) {
      return public_methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
      return public_methods.init.apply(this, arguments);
    } else {
      $.error('Method ' +  method + ' does not exist on jQuery.invoiceForm');
    } // if
  };
  
  /**
   * handle variable storage
   * 
   * @param jQuery element
   * @param Object variables
   * @return mixed
   *|*|*|*/
  var _var = function (element) {
    return element.data(plugin_name + 'Variables');
  }; // _var
  
  /**
   * Initialize variable storage
   * 
   * @param jQuery element
   * @return null
   *|*|*|*/
  var _initvar = function (element) {
    element.data(plugin_name + 'Variables', {});    
  }; // _initvar

})(jQuery);


/**
 * Validate invoice items
 *
 * @param jQuery field
 * @param String caption
 */
// window.validate_invoice_items = function(field, caption) {
//   var error_message = false;
//   var error_messages = new Array();
//   
//   var item_count = items_table.find('tr.item').length;
//   
//   items_table.find('tr.item').each(function() {
//     var row = $(this);
//     var description = jQuery.trim(row.find('td.description input').val());
//     var quantity = App.parseNumeric(row.find('td.quantity input').val());
//     var unit_cost = App.parseNumeric(row.find('td.unit_cost input').val());
//     var total = App.parseNumeric(row.find('td.total input').val());
//     
//     if (!description && !total && !unit_cost) {
//       // check if there are empty rows
//       if (item_count > 1) {
//         row.remove();
//       } // if
//     } else if (!description && (total && unit_cost)) {
//       // check if there are missing descriptions
//       error_messages[0] = App.lang('All descriptions are required.');
//     } // if
//   });
//       
//   var invoice_total = App.parseNumeric(invoice_total_field.val());
//   if (invoice_total <= 0) {
//     error_messages[1] = App.lang('Invoice total is invalid. Invoice total must be greater than zero');
//   } // if    
//   
//   if (error_messages.length > 0) {
//     error_message = error_messages.join('<br />');
//   } // if
//   return error_message ? error_message : true;
// };